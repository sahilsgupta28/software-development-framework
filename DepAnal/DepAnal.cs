﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DependencyAnalyzer
{
    using DepTable = List<TypeEntry>;

    struct TypeString
    {
        public static string namespaceStr = "namespace";

        public static string classStr = "class";

        public static string usingStr = "using";
        public static string newStr = "new";
    }

    class TypeEntry
    {
        public string type { set; get; }
        public string ns { set; get; }
        public string name { set; get; }

        public TypeEntry(string typeStr, string nameStr)
        {
            type = typeStr;
            ns = "Global";
            name = nameStr;
        }

        public TypeEntry(string typeStr, string namespaceStr, string nameStr)
        {
            type = typeStr;
            ns = namespaceStr;
            name = nameStr;
        }

        public override bool Equals(object newobj)
        {
            TypeEntry obj = newobj as TypeEntry;

            if (obj == null)
                return false;

            if (type.Equals(obj.type) && ns.Equals(obj.ns) && name.Equals(obj.name))
                return true;

            return false;
        }

        public override int GetHashCode()
        {
            return type.GetHashCode() ^ ns.GetHashCode() ^ name.GetHashCode();
        }
    }

    public class DepAnal
    {
        Dictionary<string, DepTable> dependencies;        // filename : dependency table for current file
        List<string> depTypes;

        Dictionary<string, DepTable> definitions;         // filename : type table for current file
        List<string> defTypes;        

        public DepAnal()
        {
            //
            // Dependency Table
            //
            dependencies = new Dictionary<string, DepTable>();

            depTypes = new List<string>();
            depTypes.Add(TypeString.usingStr);  // USING
            depTypes.Add(TypeString.newStr);    // NEW

            //
            // Type Table
            //
            definitions = new Dictionary<string, DepTable>();
            defTypes = new List<string>();

            defTypes.Add(TypeString.namespaceStr);  // NAMESPACE
            defTypes.Add(TypeString.classStr);      // CLASS
        }

        //
        //  Functions
        //

        public void BuildDepTable(string filename)
        {
            DepTable dep = new DepTable();
            DepTable def = new DepTable();

            using (StreamReader sr = new StreamReader(Path.GetFullPath(filename)))
            {
                string line;
                char space = ' ';
                bool comment = false;
                string ns = "GLOBAL";
                while ((line = sr.ReadLine()) != null)
                {
                    // Skip white space
                    line = line.TrimStart(space);
                    if (doSkip(line, ref comment)) {
                        continue;
                    }

                    //Console.WriteLine(line);
                    TypeEntry entry = lineParser(line);
                    if (entry != null)
                    {
                        if (entry.type.Equals(TypeString.namespaceStr))
                            ns = entry.name;
                        else
                            entry.ns = ns;

                        if (defTypes.Contains(entry.type))
                            adddefinition(ref def, ref entry);
                        if (depTypes.Contains(entry.type))
                            adddependency(ref dep, ref def, ref entry);
                    }
                }
            }

            definitions.Add(Path.GetFileName(filename), def);
            dependencies.Add(Path.GetFileName(filename), dep);
        }

        bool isClassDefined(DepTable dt, TypeEntry searchEntry)
        {
            foreach (var entry in dt)
            {
                if (entry.type.Equals(TypeString.classStr) &&  entry.ns.Equals(searchEntry.ns) && entry.name.Equals(searchEntry.name))
                    return true;
            }
            return false;
        }

        bool isNamespaceDefined(DepTable dt, TypeEntry searchEntry)
        {
            foreach (var entry in dt)
            {
                if (entry.type.Equals(TypeString.namespaceStr) && entry.name.Equals(searchEntry.name))
                    return true;
            }
            return false;
        }

        private void adddependency(ref DepTable dep, ref DepTable def, ref TypeEntry entry)
        {
            // Rule: For new <vairable>, if class <variable> is present, this is a definition so dont add as dependency
            if (entry.type == TypeString.newStr && isClassDefined(def, entry))
                return;

            //todo add if new and class
            dep.Add(entry);
        }

        private void adddefinition(ref DepTable def, ref TypeEntry entry)
        {
            def.Add(entry);
        }

        bool doSkip(string line, ref bool comment)
        {
            if (comment)
            {
                if (line.Contains(@"*/"))
                    comment = false;
                return true;
            }

            if (line.StartsWith(@"//"))
                return true;

            if (line.StartsWith(@"/*"))
            {
                comment = true;
                return true;
            }

            return false;
        }

        private TypeEntry lineParser(string line)
        {
            string[] words = line.Split(' ');
            char[] charsToTrim = {'(', ')', ';'};

            if(words.Count() == 0) {
                return null;
            }

            // NAMESPACE
            if (words[0].Equals(TypeString.namespaceStr))           //namespace NS
            {
                return new TypeEntry(TypeString.namespaceStr, words[1]);
            }

            for (int i = 0; i < words.Count(); i++)
			{
                //CLASS
                if (words[i].Equals(TypeString.classStr))          //class A
                {
                    if ((i + 1) <= words.Count())
                        return new TypeEntry(TypeString.classStr, words[i + 1]);
                    return null;
                }

                //USING
                if (words[i].Equals(TypeString.usingStr))          //using System.IO;
                {
                    if((i + 1) <= words.Count())
                    {
                        if (words[i + 1] != "=" && !words[i+1].StartsWith("System"))      //using DepTable = List<Tuple<string, string>>; using System
                            return new TypeEntry(TypeString.usingStr, words[i + 1].TrimEnd(charsToTrim));
                        return null;
                    }
                }

                //VARIABLE
                if (words[i].Equals(TypeString.newStr))          //new A();
                {
                    if ((i + 1) <= words.Count())
                        return new TypeEntry(TypeString.newStr, words[i + 1].TrimEnd(charsToTrim));
                    return null;
                }
			}

            return null;
        }

        // Get list of files on which filename depends on
        public List<string> DependsOn(string file)
        {
            List<string> files = new List<string>();
            string filename = Path.GetFileName(file);

            //Get dependency table for file
            DepTable dep;

            if (!dependencies.TryGetValue(filename, out dep))
            {
                Console.WriteLine("Error: Dependencies not found");
                return null;
            }

            //Find Dependency in definitions of other files
            //If definition is found, add to depends on list        
            foreach (var def_file in definitions)
            {
                if (def_file.Key.Equals(filename))  //Skip Current File
                    continue;

                foreach (var depentry in dep)
                {
                    if (isClassDefined(def_file.Value, depentry))
                    {
                        files.Add(def_file.Key);
                        //dep.Remove(depentry);
                    }

                    if (depentry.type.Equals(TypeString.usingStr) && isNamespaceDefined(def_file.Value, depentry))
                    {
                        files.Add(def_file.Key);
                    }
                }
            }

            return files;
        }

        //
        // DISPLAY
        //

        public void showDependencies()
        {
            foreach (var item in dependencies)
            {
                Console.WriteLine("Dependencies for File : {0}", item.Key);
                foreach (var entry in item.Value)
                {
                    Console.WriteLine("{0} : {1} : {2}", entry.type, entry.ns, entry.name);
                }
                Console.WriteLine();
            }
        }

        public void showDefinitions()
        {
            foreach (var item in definitions)
            {
                Console.WriteLine("Definitions for File : {0}", item.Key);
                foreach (var entry in item.Value)
                {
                    Console.WriteLine("{0} : {1} : {2}", entry.type, entry.ns, entry.name);
                }
                Console.WriteLine();
            }
        }

        static void Main(string[] args)
        {
            DepAnal da = new DepAnal();

            da.BuildDepTable(@"..\..\..\SampleDriver\SampleDriver.cs");
            da.BuildDepTable(@"..\..\..\SampleCode\SampleCode.cs");
            da.BuildDepTable(@"..\..\..\TestInterface\ITest.cs");
            da.showDependencies();
            da.showDefinitions();

            List<string> files = da.DependsOn(@"..\..\..\SampleDriver\SampleDriver.cs");
            Console.Write("{0} depends on : ", "SampleDriver.cs");
            foreach (string filename in files)
            {
                Console.Write("{0} ", filename);
            }
            Console.WriteLine("");
        }
    }
}
