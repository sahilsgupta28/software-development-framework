﻿/**
 * Receiver
 * Implements communication interface to receive messages
 * 
 * FileName     : Receiver.cs
 * Author       : Sahil Gupta
 * Date         : 14 November 2016 
 * Version      : 1.0
 *
 * Public Interface
 * ----------------
 * Receiver()
 *  - Constructor to initialize a new receiver
 * void Init(string url)
 *  - Initialize URL at which reciever will be listening
 * void SendMessage(Message msg)
 *  - Interface to send message
 * Message GetMessage()
 *  - Interface to get message
 *  
 * Build Process
 * -------------
 * - Required files:   ITest.cs, BlockingQueue.cs
 * 
 * Maintenance History
 * -------------------
 * ver 1.0 : 14 November 2016
 *  - first release
 */

using System;
using System.ServiceModel;
using nsBlockingQueue;
using TestInterface;

namespace Communication
{
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerSession)]
    public class Receiver : ICommunication
    {
        ServiceHost host = null;
        static BlockingQueue<Message> BlockingQ = null;

        public Receiver()
        {
            Console.WriteLine("Starting Receiver...");
            if (BlockingQ == null)
            {
                BlockingQ = new BlockingQueue<Message>();
            }
        }

        ~Receiver()
        {
            if (host != null)
            {
                host.Close();
            }
            
            Console.WriteLine("Stopping Receiver...");
        }

        public void Init(string url)
        {
            Console.WriteLine("Creating WCF Reciever for URL({0})", url);
            WSHttpBinding WsBinding = new WSHttpBinding();
            Uri IMsgPassingUri = new Uri(url);
            host = new ServiceHost(typeof(Receiver), IMsgPassingUri);
            host.AddServiceEndpoint(typeof(ICommunication), WsBinding, IMsgPassingUri);
            //start listening to messages
            host.Open();
        }

        public void SendMessage(Message msg)
        {
            BlockingQ.enQ(msg);
        }

        public Message GetMessage()
        {
            return BlockingQ.deQ();
        }

        static void Main(string[] args)
        {
            Receiver MyReceiver = null;

            try
            {
                MyReceiver = new Receiver();

                string url = @"http://localhost:4000/TestHarness/ICommunication";
                MyReceiver.Init(url);

                Console.WriteLine("Starting Receiver...\n");

                while (true)
                {
                    Console.WriteLine("Waiting For Message...");
                    Message Msg = MyReceiver.GetMessage();
                    Console.WriteLine("New Message Received:");
                }
            }
            catch (Exception Ex)
            {
                Console.WriteLine("Exception : {0}", Ex.Message);
            }
        }
    }
}