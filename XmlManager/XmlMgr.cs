﻿/*************************************************************
    //
    //XML Structure
    //

    <?xml version="1.0" encoding="UTF-8" standalone="true"?>
    <!--Repository-->
    <RepositoryLog>
      <Commit ID="1001">
        <TimeStamp>11/21/2018 8:16:41 PM</TimeStamp>
        <Title>Second</Title>
        <Description>Added new files</Description>
        <Files>
            <File>file3.cs</File>
            <File>file4.cs</File>
        </Files>
        <Build Version="1.0.0.1">file1.lib</Build>
      </Commit>
    </RepositoryLog>
**************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XmlManager
{
    using System.Xml;
    using System.Xml.Linq;
    using System.IO;

    public class XmlMgr
    {
        protected XDocument xDoc;
        protected XElement xRoot;

        string XmlFileName;
        string XmlDir;

        bool FlushOnWrite;

        public XmlMgr(string filename, string filedir)
        {
            XmlFileName = filename;
            XmlDir = filedir;
            FlushOnWrite = true;

            CreateorOpenXml();
        }

        ~XmlMgr()
        {
            FlushAll();
        }

        private void CreateorOpenXml()
        {
            string FilePath = Path.Combine(XmlDir, XmlFileName);

            if (File.Exists(FilePath))
            {
                Console.WriteLine("Opening File {0}", FilePath);
                xDoc = XDocument.Load(FilePath);

                xRoot = xDoc.Root;
            }
            else
            {
                Console.WriteLine("Creating File {0}", FilePath);
                xDoc = new XDocument();
                xDoc.Declaration = new XDeclaration("1.0", "utf-8", "yes");

                XComment comment = new XComment("Repository");
                xDoc.Add(comment);

                xRoot = new XElement("RepositoryLog");
                xDoc.Add(xRoot);

                xDoc.Save(FilePath);
            }
        }

        protected void FlushAll()
        {
            if (!FlushOnWrite)
                return;

            string FilePath = Path.Combine(XmlDir, XmlFileName);
            xDoc.Save(FilePath);
        }

        public void Display()
        {
            Console.WriteLine("\n=== XML ===");
            Console.WriteLine("{0}", xDoc.Declaration);
            Console.WriteLine("{0}", xDoc.ToString());
            Console.WriteLine("=== XML ===");
        }

        public void SearchTagName(string tagName)
        {
            IEnumerable<XElement> allElements = xDoc.Descendants(tagName);
            foreach (var elem in allElements)
                Console.WriteLine("\n{0}", elem.ToString());
        }

        public void SearchAttribName(string AttribName)
        {
            IEnumerable<XElement> allElements = xDoc.Root.Descendants();
            foreach (var elem in allElements)
            {
                if (elem.Attributes().Count() > 0)
                    if (elem.Attribute(AttribName) != null)
                        Console.WriteLine("\n{0}", elem.ToString());
            }
        }
            
        public List<XElement> SearchAttribNameValue(string tag, string AttribName, string AttribVal)
        {
            List<XElement> elems = new List<XElement>();

            IEnumerable<XElement> allElements = xDoc.Root.Descendants(tag);
            foreach (var elem in allElements)
            {
                if (elem.Attributes().Count() > 0)
                    if (elem.Attribute(AttribName).Value.Equals(AttribVal)) {
                        //Console.WriteLine("\n{0}", elem.ToString());
                        elems.Add(elem);
                    }
            }
            
            return elems;
        }

        public void SearchEleVal(string SearchVal)
        {
            IEnumerable<XElement> allElements = xDoc.Root.Descendants();
            foreach (var elem in allElements)
            {
                if (elem.Value != null)
                    if (elem.Value.Contains(SearchVal))
                        Console.WriteLine("\n{0}", elem.ToString());
            }
        }
    };

    public class RepoXml : XmlMgr
    {
        //
        //  Repository Support
        //
        public RepoXml(string filename, string filedir) 
            : base(filename, filedir) {

        }

        public bool AddNewRecord(string commitId, string author, string title, string desc, List<string> files)
        {
            // Check if ID exists
            List<XElement> elems = SearchAttribNameValue("Commit", "ID", commitId);
            if (elems.Count != 0)
            {
                Console.WriteLine("Record with commit id {0} alread exists", commitId);
                return false;
            }

            //Add new commit node
            XElement xCommit = new XElement("Commit");
            xCommit.SetAttributeValue("ID", commitId);

            xCommit.Add(new XElement("TimeStamp", DateTime.Now.ToString()));
            xCommit.Add(new XElement("Title", title));
            xCommit.Add(new XElement("Description", desc));

            XElement xFiles = new XElement("Files");
            foreach (string file in files)
            {
                //todo send only file name from client on phase 2 of commit.
                xFiles.Add(new XElement("File",Path.GetFileName(file)));
            }
            xCommit.Add(xFiles);

            Console.WriteLine("Added new record with ID : {0}", commitId);
            xRoot.AddFirst(xCommit);

            FlushAll();

            return false;
        }

        public bool AddBuildToCommit(string commitId, string buildVersion, List<string> libList)
        {
            //Get commit node
            XElement xCommit = getCommitRecord(commitId);
            if (xCommit == null)
            {
                Console.WriteLine("getCommitRecord...FAILED.");
                return false;
            }
            Console.WriteLine("Found Commit ID({0}) => Title({1})", commitId, xCommit.Element("Title").Value);

            IEnumerable<XElement> commitElements = xCommit.Descendants("Build");

            foreach (string libName in libList)
            {
                // Check if build already exists for library
                foreach (var elem in commitElements)
                {
                    if (elem.Value.Equals(libName))
                    {
                        Console.WriteLine("Commit ({0}) already contains a build version ({1}) for lib ({2})",
                            commitId, elem.Attribute("Version").Value, libName);
                        return false;
                    }
                }

                //Add new build element
                XElement xBuild = new XElement("Build", libName);
                xBuild.SetAttributeValue("Version", buildVersion);

                Console.WriteLine("Added new build to commit {0}", commitId);
                xCommit.Add(xBuild);
            }

            FlushAll();

            return true;
        }

        //  Search Functions

        XElement getCommitRecord(string commitId)
        {
            List<XElement> elems = SearchAttribNameValue("Commit", "ID", commitId);
            if (elems.Count == 0 || elems.Count > 1)
            {
                Console.WriteLine("SearchAttribNameValue...FAILED. Count({0})", elems.Count);
                return null;
            }

            return elems.First();
        }

        public List<string> getFilesForCommit(string commitId)
        {
            List<string> files = new List<string>();

            XElement xCommit = getCommitRecord(commitId);
            if (xCommit == null)
            {
                return files;
            }

            IEnumerable<XElement> xFiles = xCommit.Descendants("File");
            foreach (var elem in xFiles)
            {
                files.Add(elem.Value);
            }

            return files;
        }

        public List<string> getLibrariesForCommit(string commitId)
        {
            List<string> files = new List<string>();

            XElement xCommit = getCommitRecord(commitId);
            if (xCommit == null)
            {
                return files;
            }

            IEnumerable<XElement> xFiles = xCommit.Descendants("Build");
            foreach (var elem in xFiles)
            {
                files.Add(elem.Value);
            }

            return files;
        }

        public string getLatestBuildVersionForLib(string libname)
        {
            IEnumerable<XElement> xBuildElements = xDoc.Root.Descendants("Build");
            foreach (var elem in xBuildElements)
            {
                if (elem.Value.Contains(libname)) {
                    return elem.Attribute("Version").Value;
                }
            }

            return null;
        }

        public string getLatestCommitForFile(string filename)
        {
            for (XNode xCommit = xDoc.Root.FirstNode; xCommit != null; xCommit = xCommit.NextNode)
            {
                IEnumerable<XElement> xFiles = (xCommit as XElement).Descendants("File");
                foreach (var elem in xFiles)
                {
                    if (elem.Value.Equals(filename))
                    {
                        return (xCommit as XElement).Attribute("ID").Value;
                    }
                }
            }

            return null;
        }

        public string getCommitIdForBuild(string buildVersion, string libname)
        {
            for (XNode xCommit = xDoc.Root.FirstNode; xCommit != null; xCommit = xCommit.NextNode)
            {
                IEnumerable<XElement> xFiles = (xCommit as XElement).Descendants("Build");
                foreach (var elem in xFiles)
                {
                    if (elem.Value.Equals(libname) && elem.Attribute("Version").Value.Equals(buildVersion))
                    {
                        return (xCommit as XElement).Attribute("ID").Value;
                    }
                }
            }

            return null;
        }

        public string getLastCommitId()
        {
            XNode FirstCommit = xDoc.Root.FirstNode;
            if (FirstCommit == null)
                return null;

            return (FirstCommit as XElement).Attribute("ID").Value;
        }

        public List<string> getAllCommitId()
        {
            List<string> CommitIDs = new List<string>();

            for (XNode xCommit = xDoc.Root.FirstNode; xCommit != null; xCommit = xCommit.NextNode)
            {
                CommitIDs.Add((xCommit as XElement).Attribute("ID").Value);
            }
            
            return CommitIDs;
        }

        static void Main(string[] args)
        {
            RepoXml xml = new RepoXml("RepositoryStore.xml", "..\\..\\..\\Repository\\RepositoryServerDir");

            xml.AddNewRecord("1000", "Sahil", "First", "NA", new List<string> { "file1.cs", "file2.cs" });
            xml.AddNewRecord("1001", "Sahil", "Second", "Added new files", new List<string> { "file3.cs", "file4.cs" });
            xml.AddNewRecord("1002", "Sahil", "Refactor", "Changed 1,2", new List<string> { "file1.cs", "file4.cs" });
            
            xml.AddBuildToCommit("1000", "1.0.1000.1", new List<string> {"file1.lib"});
            xml.AddBuildToCommit("1001", "1.0.1001.1", new List<string> {"file1.lib"});

            xml.Display();

            Console.WriteLine("Latest Build for file1.lib : " + xml.getLatestBuildVersionForLib("file1.lib"));

            Console.WriteLine("Last Commit ID : " + xml.getLastCommitId());
        }
    }
}
