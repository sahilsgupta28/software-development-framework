﻿/**
 * CLI
 * Implements command line interface to accept test requests
 * 
 * FileName     : CLI.cs
 * Author       : Sahil Gupta
 * Date         : 24 September 2016 
 * Version      : 1.0
 * 
 * Usage
 * -------
 *      CLI.exe /r repository_path /t testrequest1_path [(...)]
 *      CLI.exe /r repository_path /q testrequest1_path
 *      CLI.exe /r repository_path /a AuthorName
 *      CLI.exe /r repository_path /s 
 *      
 *      /r Repository   - Path to Repository containing assemblies to be tested
 *      /t Test Request - XML file specifying test code
 *      /q Query        - Get test result of specific Test Request
 *      /a Author       - Get test results of a particular author
 *      /s Summary      - Get Summary of all tests executed
 * 
 * Public Interface
 * ----------------
 * static void TestRequest(ref string[] args)
 * 
 * Build Process
 * -------------
 * - Required files:   TestExec.cs
 * 
 * Maintenance History
 * -------------------
 * ver 1.0 : 24 September 2016
 *  - first release
 */

using System;
using System.Diagnostics;
using System.IO;


namespace UI
{
    using TestHarness;

    class CLI
    {

        public static void TestRequest(ref string[] args)
        {
            try {
                string Myurl = @"http://localhost:4000/ICommunication/TestHarness";
                string LocalRepoPath = @"..\..\..\Repository\TestHarnessDir";
                string RepoUrl = @"http://localhost:4000/ICommunication/Repository";
                string FileHostingUrl = @"http://localhost:4000/IStream/FileHosting";
                /* Instantiate new Test Executive to process Test Requests */
                TestExec testexe = new TestExec(Myurl, LocalRepoPath);
                testexe.configureRepository(RepoUrl, FileHostingUrl);
                switch (args[2][1]) {
                    case 't': /* Client provides Test Request to process */
                        int ReqCnt = 3; // Position of 1st Test Request in Command Line Argument string "args"                
                        do {
                            //testexe.EnqueueTestRequest(args[ReqCnt]); /* Enqueue test request */
                        } while (++ReqCnt != args.Length);
                        testexe.ProcessTestRequests(); /* Process all queued test requests */
                        break;
                    case 'q': /* Client wants test result of a particular test request */
                        testexe.GetTestRequestResult(args[3]);
                        break;
                    case 'a': /* Client wants test results of his tests */
                        testexe.GetAuthorTestResult(args[3]);
                        break;
                    case 's': /* Client wants summary of all test requests */
                        testexe.GetTestsSummary();
                        break;
                    default:
                        Console.WriteLine("Invalid Argument");
                        break;
                }
            } catch (Exception Ex) {
                Console.WriteLine("Exception: {0}", Ex.Message);
            }
        }

        static void Main(string[] args)
        {

            /*  Validate input 
             *  We Expect Repository Path as first input followed by one or more test requests
             */
            if (args.Length < 3)
            {
                Console.WriteLine("Error: The syntax of the command is incorrect.");
                return;
            }

            if (@"/r" != args[0])
            {
                Console.WriteLine("Error: Must have Repository Path");
                return;
            }

            if (2 != args[2].Length)
            {
                Console.WriteLine("Error: Invalid arguemnt");
                return;
            }

            /* Check if Repository is valid */
            if (!Directory.Exists(args[1]))
            {
                Console.WriteLine("Error: Invalid Path {0}", args[1]);
                return;
            }

            TestRequest(ref args);
        }
    }
}