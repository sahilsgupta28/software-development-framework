﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace BuildServer
{
    using TestInterface;
    using Communication;
    using nsBlockingQueue;
    using FileHosting;
    using FileManager;
    using System.IO;
    using System.Diagnostics;
    using DependencyAnalyzer;

    class BuildSrv
    {
        string URL;

        //string LocalRepo;
        //string RepoServerUrl;
        //string FileHostingUrl;

        Thread tSender;
        Sender MySender;
        BlockingQueue<Message> SendQ;

        Thread tReceiver;
        Receiver MyReceiver;
        
        BlockingQueue<Message> RequestQ;
        BlockingQueue<Message> ReadyQ;

        int childProcessId;
        int childProcessMax;
        string childProcessLocation;
        string childProcessUrlPrefix;

        public BuildSrv(string MyUrl)
        {
            Console.WriteLine("Starting Build Server at URL ({0})", MyUrl);
            this.URL = MyUrl;

            childProcessId = 0;
            childProcessMax = 2;
            childProcessLocation = Path.GetFullPath("Builder/bin/Release/Builder.exe");
            childProcessUrlPrefix = @"http://localhost:4000/ICommunication/Builder";

            SendQ = new BlockingQueue<Message>();
            MySender = new Sender();
            tSender = new Thread(ThreadSender);
            tSender.Start();

            RequestQ = new BlockingQueue<Message>();
            ReadyQ = new BlockingQueue<Message>();

            MyReceiver = new Receiver();
            tReceiver = new Thread(ThreadReceiver);
            tReceiver.Start();

            initProcessPool();
        }

        ~BuildSrv()
        {
            /* Stop Reciever first because if we stop sender and we recieve a msg,
             * then we cannot send the results of that msg */
            tReceiver.Join();

            tSender.Join();
        }

        //public void configureBuildServer(string LocalRepo, string RepoUrl, string FileHostingUrl)
        //{
        //    this.LocalRepo = LocalRepo;
        //    this.RepoServerUrl = RepoUrl;
        //    this.FileHostingUrl = FileHostingUrl;
        //}

        void ThreadSender()
        {
            while (true)
            {
                Message msg = SendQ.deQ();

                if (msg.Type == Message.MessageType.Quit)
                {
                    while (ReadyQ.size() > 0)
                    {
                        Message ReadyMsg = ReadyQ.deQ();
                        msg.ToUrl = ReadyMsg.FromUrl;
                        Console.WriteLine("\nSending WCF Message ({0}) to ({1}).", msg.Type.ToString(), msg.ToUrl);
                        MySender.CreateNewSession(msg.ToUrl);
                        MySender.SendMessage(msg);
                    }
                    Console.WriteLine("Quitting...");
                    return;
                }

                Console.WriteLine("\nSending WCF Message ({0}) to ({1}).", msg.Type.ToString(), msg.ToUrl);
                MySender.CreateNewSession(msg.ToUrl);
                MySender.SendMessage(msg);
            }
        }

        void ThreadReceiver()
        {
            Message Msg;

            MyReceiver.Init(URL);

            Console.WriteLine("Waiting for Messages...");
            while (true)
            {
                //TODO : Delegate Work to another thread
                Msg = MyReceiver.GetMessage();
                
                Console.WriteLine("PROCESSING START ({0}) from ({1}).", Msg.Type.ToString(), Msg.FromUrl);
                
                switch (Msg.Type) {

                    case Message.MessageType.BuildRequest:
                        RequestQ.enQ(Msg);
                        break;

                    case Message.MessageType.Ready:
                        ReadyQ.enQ(Msg);
                        break;

                    case Message.MessageType.Quit:
                        SendQ.enQ(Msg);
                        return;

                    default:
                        Console.WriteLine("Unknown Message ({0}) Received:", Msg.Type.ToString());
                        break;
                }

                dispatchMessage();

                Console.WriteLine("PROCESSING END ({0})", Msg.Type.ToString());
            }
        }

        void initProcessPool()
        {
            for (int i = 0; i < childProcessMax; i++)
            {
                createChildProcess(childProcessId++);
            }
        }

        void createChildProcess(int i)
        {
            Process p = new Process();
            string childProcessUrl = childProcessUrlPrefix + i.ToString();
            string args = childProcessUrl + " " + URL;
            Process.Start(childProcessLocation, args);
        }

        bool dispatchMessage()
        {
            if (ReadyQ.size() > 0 && RequestQ.size() > 0)
            {
                Message ReadyMsg = ReadyQ.deQ();
                Message RequestMessage = RequestQ.deQ();
                RequestMessage.ToUrl = ReadyMsg.FromUrl;
                SendQ.enQ(RequestMessage);
                return true;
            }
            return false;
        }

        static void Main(string[] args)
        {
            string URL = @"http://localhost:4000/ICommunication/BuildServer";
            //string RepoUrl = @"http://localhost:4000/ICommunication/Repository";
            //string FileHostingUrl = @"http://localhost:4000/IStream/FileHosting";
            //string LocalRepositoryPath = "Repository\\BuildSrvDir";

            Console.Title = "Build Server (" + URL + ")";

            BuildSrv builder = new BuildSrv(URL);
            //builder.configureBuildServer(LocalRepositoryPath, RepoUrl, FileHostingUrl);

            //// Build Order
            //BuildOrder bo = new BuildOrder();
            //Dictionary<string, List<string>> dep = new Dictionary<string, List<string>>();
            //dep.Add("a", new List<string> { "f" });
            //dep.Add("b", new List<string> { "f" });
            //dep.Add("c", new List<string> { "d" });
            //dep.Add("d", new List<string> { "a", "b" });
            //dep.Add("e", new List<string> ());
            //dep.Add("f", new List<string> ());
            
            //List<string> order = bo.getBuildOrder(dep);
           
            //Console.Write("Build Order : ");
            //foreach (string name in order)
            //    Console.Write("{0} ", name);
            //Console.WriteLine("");

        }
    }
}