﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BuildServer
{

    public class Project
    {
        public String Name { set; get; }
	    public int dependencies  { set; get; }
        public List<Project> children;
	    //private Dictionary<String, Project> map;

	    public Project (String n) 
        {
            Name = n;
            dependencies = 0;

            children = new List<Project>();
            //map = new Dictionary<String, Project>();
        }

        // Node depends on current project
        // Add node as child of current project and increment dependency of node
	    public void addDependency(Project dependant)
        {
            //if (!map.ContainsKey(dependant.Name)) {
                children.Add(dependant);
                dependant.incrementDependencies();
		    //}
	    }

    	public void incrementDependencies() {	++dependencies;	 }
	    public void decrementDependencies() {	--dependencies;	 }
    }

    public class Graph
    {
	    private List<Project> nodes;
	    private Dictionary<String, Project> map;

        public Graph()
        {
            nodes = new List<Project>();
	        map = new Dictionary<String, Project>();
        }

	    public Project getNode(String name)
        {
            Project node;
		    
            if(!map.TryGetValue(name, out node))
            {
			    node = new Project(name);
			    nodes.Add(node);
			    map.Add(name, node);
            }

		    return node;
	    }

        public void addDependency(String dependent, String independent)
        {
		    Project i = getNode(independent);
		    Project d = getNode(dependent);
		    i.addDependency(d);
	    }

	    public List<Project> getNodes()
        {
            return nodes;
        }
    }


    class BuildOrder
    {
        public List<string> getBuildOrder(Dictionary<string, List<string>> dependencies)
        {
            Graph graph = buildGraph(dependencies);
            List<Project> order = orderProjects(graph.getNodes());

            List<string> s = new List<string>();
            foreach (Project p in order) {
                s.Add(p.Name);
            }

            return s;
        }

        Graph buildGraph(Dictionary<string, List<string>> dependencies)
        {
	        Graph graph = new Graph();
            Project p;

	        foreach (var dep in dependencies)
            {
                p = graph.getNode(dep.Key);
                foreach (string prj in dep.Value)
                    graph.addDependency(dep.Key, prj);
            }

	        return graph;
        }

        List<Project> orderProjects(List<Project> projects) 
        {
	        List<Project> ordered = new List<Project>(projects.Capacity);

            // First add all independent projects to build order
            addNonDependent(ref ordered, ref projects);

            for (int toBeProcessed = 0; toBeProcessed < ordered.Count; ++toBeProcessed)
            {
                Project current = ordered.ElementAtOrDefault(toBeProcessed);
                if (current == null)
                    return null;		// circular dependency, no remaining projects without dependencies

                // Now that independent project is built, remove it as dependency from its childs
                foreach (Project child in current.children)
                    child.decrementDependencies();

                // If the child has no more dependents, add it to build order
                addNonDependent(ref ordered, ref current.children);
            }

            return ordered;
        }

        // Insert projects without dependencies into the order array
        void addNonDependent(ref List<Project> order, ref List<Project> projects)
        {
            foreach (Project project in projects)
                if (project.dependencies == 0)
                    order.Add(project);
        }
    }
}