﻿/**
 * Interface
 * Defines ITest Interface which ever test driver must implement
 * 
 * FileName     : ITest.cs
 * Author       : Sahil Gupta
 * Date         : 25 September 2016 
 * Version      : 2.0
 * 
 * Public Interface
 * ----------------
 * ITest
 *  - To test code and get logs
 * ICommunication
 *  - to send messages 
 * IStream
 *  - to send files
 *  
 * Build Process
 * -------------
 * - Required files:   NA
 * 
 * Maintenance History
 * -------------------
 * ver 1.0 : 25 September 2016
 *  - first release
 * ver 2.0 : 20 November 2016
 *  - Added interface to support commmunication between processes to send test request and files
 *  - Added interfaces to support file streaming
 */

using System;
using System.IO;
using System.ServiceModel;
using System.Runtime.Serialization;

namespace TestInterface
{
    public interface ITest
    {
        bool test();
        string getLog();
    }

    [ServiceContract]
    public interface ICommunication
    {
        [OperationContract(IsOneWay = true)]
        void SendMessage(Message msg);

        Message GetMessage();
    }

    [DataContract]
    public class Message
    {
        public enum MessageType
        {
            [EnumMember]
            Quit,               //Sent by Client to Repository to get info

            [EnumMember]
            RepoInfo,            //Sent by Client to Repository to get info
            [EnumMember]
            GetCommitInfo,
            [EnumMember]
            CommitBegin,         //Sent by Client to Repository to start commit and get commit id
            [EnumMember]
            CommitInitiated,     //Sent by Repository in response to CommitBegin with commit id
            [EnumMember]
            CommitEnd,           //Sent by Client to Repository to indicated end of commit after uploading files
            [EnumMember]
            CommitCompleted,     //Sent by Repository in response to CommitEnd

            [EnumMember]
            Checkout,            //Sent by Client to Repository to checkout files/commit/build

            [EnumMember]
            BuildRequest,        //Sent by Client to Build Server to build source
            [EnumMember]
            BuildAck,            //Sent by Build Server to Client in response to Build Request
            [EnumMember]
            StoreBuildLogMsg,    //Sent by Build Server to Repository to store build result
            [EnumMember]
            Ready,               //Sent by Builder to Build Server to indicate task completion

            [EnumMember]
            TestRequestMessage, //Sent by Client to Test Harness to test given DLL
            [EnumMember]
            TestRequestAck,     //Sent by Test Harness to Client in response to TestRequestMessage
            [EnumMember]
            StoreTestLogMsg,    //Sent by Test Harness to Repository to store test results

            [EnumMember]
            GetTestLogMsg,      //Sent by Client to Repository to retrieve test results
            [EnumMember]
            TestLogsAck,        //Sent by Repository to Client in response to GetTestLogMsg
        }

        [DataMember]
        public string FromUrl { get; set; }

        [DataMember]
        public string ToUrl { get; set; }

        [DataMember]
        public DateTime TimeStamp { get; set; }

        [DataMember]
        public string Author { get; set; }

        [DataMember]
        public MessageType Type { get; set; }

        [DataMember]
        public string Payload { get; set; }
    }

    [ServiceContract]
    public interface IStream
    {
        [OperationContract(IsOneWay = true)]
        void Upload(FileTransferMessage msg);

        [OperationContract]
        Stream Download(string filename, string tag = null);

        [OperationContract]
        bool CheckFileAvailability(string filename, string tag = null);
    }

    [MessageContract]
    public class FileTransferMessage
    {
        [MessageHeader(MustUnderstand = true)]
        public string Filename { get; set; }

        [MessageHeader]
        public string Tag { get; set; }

        [MessageBodyMember(Order = 1)]
        public Stream TransferStream { get; set; }
    }

}
