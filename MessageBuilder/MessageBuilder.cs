﻿/**
 * Message Builder
 * Functions to Creates a new message
 * Test Message denoting all fields of test request to be included in message payload while communicating
 * 
 * FileName     : MessageBuilder.cs
 * Author       : Sahil Gupta
 * Date         : 12 October 2016 
 * Version      : 1.0
 * 
 * Public Interface for Message Builder
 * -------------------------------------
 * MessageBuilder(Message.MessageType Type)
 *  - Initialize new message builder
 * Message BuildMessage(string from, string to, string author, string payload)
 *  - Initialize fields of message
 * Message GetMessage()
 *  - Get message object
 * void DisplayMessage(Message msg)
 *  - Display message fields on console
 * 
 * Public Interface for TestMessage
 * ----------------
 * TestMessage()
 *  - new test message
 * TestMessage(string name)
 *  - new test message with test name
 * void addDriver(string name)
 *  - add driver in test message
 * void addCode(string name)
 *  - add code in test message
 * 
 * string GetTestRequestXML()
 *  - build new XML for test request
 * void ParseTestRequestXML(string TestRequestXmlString)
 *  - parse XML for extracting test request
 * string QueryTestResultXML()
 *  - build new XML to query test result
 * void ParseQueryTestResultXML(string TestResultXML)
 *  - parse XML for extracting query for test result
 * string GetTestResultXML(bool status, string Log)
 *  - build XML for test result
 * void ParseTestResultXML(string TestResultXML)
 *  - parse XML to extract test result
 *  
 * void DisplayResult()
 *  - display test message on console
 * string GetDetailedTestResult()
 *  - get string with all test message fields values
 * 
 * Build Process
 * -------------
 * - Required files:   ITest.cs, XMLParser.cs
 * 
 * Maintenance History
 * -------------------
 * ver 1.0 : 12 October 2016 
 *  - first release
 */

using System;
using System.Linq;
using System.Xml.Linq;
using System.IO;
using System.Collections.Generic;

namespace Communication
{
    using TestInterface;
    using XMLParser;
    using System.Text;
     
    public class MessageBuilder
    {
        Message msg;

        public MessageBuilder(Message.MessageType Type)
        {
            msg = new Message();
            msg.Type = Type;
        }

        public Message BuildMessage(string from, string to, string author, string payload)
        {
            msg.FromUrl = from;
            msg.ToUrl = to;
            msg.TimeStamp = DateTime.Now;
            msg.Author = author;
            msg.Payload = payload;

            return msg;
        }

        public Message GetMessage()
        {
            return msg;
        }
        
        public static void DisplayMessage(Message msg)
        {
            Console.WriteLine("From : {0}",msg.FromUrl);
            Console.WriteLine("To : {0}", msg.ToUrl);
            Console.WriteLine("TimeStamp : {0}", msg.TimeStamp);
            Console.WriteLine("Author : {0}", msg.Author);
            Console.WriteLine("Type: {0}", msg.Type.ToString());
            Console.WriteLine("Payload : {0}", msg.Payload);
        }

        static void Main(string[] args)
        {
            MessageBuilder TestRequestMsg = new MessageBuilder(Message.MessageType.TestRequestMessage);

            string payload = "TestRequest1";
            TestRequestMsg.BuildMessage("localhost", "localhost", "Sahil", payload);

            Console.WriteLine("Message Type : {0}", TestRequestMsg.GetMessage().Type.ToString());
            Console.WriteLine("Payload : {0}", TestRequestMsg.GetMessage().Payload);
        }
    }

    public class TestMessage
    {
        public int Version { get; set; }
        public string Author { get; set; }
        public DateTime TimeStamp { get; set; }
        public TimeSpan duration { get; set; }
        public string TestName { get; set; }
        public string refCommitId { get; set; }
        public string BuildVersion { get; set; }
        public string TestDriver { get; set; }
        public List<string> TestCodes { get; set; }
        public List<string> TestSource { get; set; }
        public bool TestStatus { get; set; }
        public string TestResult { get; set; }
        //string DateTimeFormat = "yyMMdd-HHmmss-fff";

        public TestMessage()
        {
            TimeStamp = DateTime.Now;
            TestCodes = new List<string>();
            TestSource = new List<string>();
        }

        public TestMessage(string name)
        {
            TestName = name;
            TimeStamp = DateTime.Now;
            TestCodes = new List<string>();
            TestSource = new List<string>();
        }

        public void addDriver(string name)
        {
            TestDriver = name;
        }

        public void addCode(string name)
        {
            TestCodes.Add(name);
        }

        public void addSource(string name)
        {
            TestSource.Add(name);
        }

        public string GetTestRequestXML()
        {
            string TestRequest;

            TestRequest = "<?xml version=\"1.0\" encoding=\"utf-8\" ?>";
            TestRequest += "<TestRequest>";
            TestRequest += "<Version>1</Version>";
            TestRequest += "<Author>" + Author + "</Author>";
            TestRequest += "<TimeStamp>" + TimeStamp + "</TimeStamp>";
            TestRequest += "<Test Name=\"" + TestName + "\">";
            TestRequest += "<CommitId>" + refCommitId + "</CommitId>";
            TestRequest += "<BuildVersion>" + BuildVersion + "</BuildVersion>";
            TestRequest += "  <TestDriver>" + Path.GetFileName(TestDriver) + "</TestDriver>";
            foreach (string code in TestCodes)
                TestRequest += "  <Library>" + Path.GetFileName(code) + "</Library>";
            foreach (string src in TestSource)
                TestRequest += "  <Source>" + Path.GetFileName(src) + "</Source>";
            TestRequest += "</Test>";
            TestRequest += "</TestRequest>";

            return TestRequest;
        }

        public void ParseTestRequestXML(string TestRequestXmlString)
        {
            //call XML parser to parse string
            XmlTesterParser Parser = new XmlTesterParser();
            Parser.ParseXmlString(TestRequestXmlString);

            //fill test message structure with parsed info
            Version = Parser._xmlTestInfoList[0]._Version;
            Author = Parser._xmlTestInfoList[0]._Author;
            TimeStamp = Parser._xmlTestInfoList[0]._TimeStamp;
            TestName = Parser._xmlTestInfoList[0]._TestName;
            refCommitId = Parser._xmlTestInfoList[0]._refCommitId;
            BuildVersion = Parser._xmlTestInfoList[0]._BuildVersion;
            TestDriver = Parser._xmlTestInfoList[0]._TestDriver;

            foreach (var Library in Parser._xmlTestInfoList[0]._TestCode)
            {
                TestCodes.Add(Library);
            }

            foreach (var Source in Parser._xmlTestInfoList[0]._TestSrc)
            {
                TestSource.Add(Source);
            }
        }

        public string QueryTestResultXML()
        {
            string TestResultXml;
            TestResultXml = "<?xml version=\"1.0\" encoding=\"utf-8\" ?>";
            TestResultXml += "<TestQuery>";
            TestResultXml += "<Author>" + Author + "</Author>";
            TestResultXml += "<TestName>" + TestName + "</TestName>";
            TestResultXml += "</TestQuery>";
            return TestResultXml;
        }

        public void ParseQueryTestResultXML(string TestResultXML)
        {
            XDocument _xDoc = XDocument.Parse(TestResultXML);
            Author = _xDoc.Descendants("Author").First().Value;
            TestName = _xDoc.Descendants("TestName").First().Value;
        }

        public string GetTestResultXML(bool status, string Log)
        {
            string TestResultXml;

            TestResultXml = "<?xml version=\"1.0\" encoding=\"utf-8\" ?>";
            TestResultXml += "<TestResult>";
            TestResultXml += "<Author>" + Author + "</Author>";
            TestResultXml += "<TimeStamp>" + TimeStamp + "</TimeStamp>";
            TestResultXml += "<Test Name=\"" + TestName + "\">";
            TestResultXml += "  <TestDriver>" + Path.GetFileName(TestDriver) + "</TestDriver>";
            foreach (string code in TestCodes)
            {
                TestResultXml += "  <Library>" + Path.GetFileName(code) + "</Library>";
            }
            foreach (string src in TestSource)
            {
                TestResultXml += "  <Source>" + Path.GetFileName(src) + "</Source>";
            }
            TestResultXml += "<TestStatus>" + status.ToString() + "</TestStatus>";
            TestResultXml += "<Log>" + Log + "</Log>";
            TestResultXml += "</Test>";
            TestResultXml += "</TestResult>";

            return TestResultXml;
        }

        public void ParseTestResultXML(string TestResultXML)
        {
            XDocument _xDoc = XDocument.Parse(TestResultXML);
            Author = _xDoc.Descendants("Author").First().Value;
            TimeStamp = DateTime.Parse(_xDoc.Descendants("TimeStamp").First().Value);
            XElement[] xElement = _xDoc.Descendants("Test").ToArray();

            foreach (var item in xElement)
            {
                TestName = item.Attribute("Name").Value;
                TestDriver = item.Element("TestDriver").Value;
                IEnumerable<XElement> xTestCode = item.Elements("Library");
                foreach (var library in xTestCode)
                {
                    TestCodes.Add(library.Value);
                }
                IEnumerable<XElement> xTestSrc = item.Elements("Source");
                foreach (var src in xTestSrc)
                {
                    TestSource.Add(src.Value);
                }
                TestStatus = Convert.ToBoolean(item.Element("TestStatus").Value);
                TestResult = item.Element("Log").Value;
            }
        }

        public void DisplayResult()
        {
            Console.WriteLine("  {0,-12} : {1}", "Version", Version);
            Console.WriteLine("  {0,-12} : {1}", "Author", Author);
            Console.WriteLine("  {0,-12} : {1}", "TimeStamp", TimeStamp);
            Console.WriteLine("  {0,-12} : {1}", "Latency", duration);
            Console.WriteLine("  {0,-12} : {1}", "TestName", TestName);
            Console.WriteLine("  {0,-12} : {1}", "CommitId", refCommitId);
            Console.WriteLine("  {0,-12} : {1}", "BuildVersion", BuildVersion);
            Console.WriteLine("  {0,-12} : {1}", "TestDriver", TestDriver);
            foreach (string Library in TestCodes)
            {
                Console.WriteLine("  {0,-12} : {1}", "Library", Library);
            }
            foreach (string Src in TestSource)
            {
                Console.WriteLine("  {0,-12} : {1}", "Source", Src);
            }
            Console.WriteLine("  {0,-12} : {1}", "TestStatus", (TestStatus ? "PASS" : "FAIL"));
            Console.WriteLine("-----------Test Result-----------");
            Console.WriteLine("{0}", TestResult);
            Console.WriteLine("--------------------------------");
            Console.WriteLine("");
        }

        public string GetDetailedTestResult()
        {
            string Log= null;

            Log += "Author : " + Author;
            Log += "\nTestName : " + TestName;
            Log += "\nTimeStamp : " + TimeStamp;
            Log += "\nTestDriver : " + TestDriver;
            foreach (string Library in TestCodes)
                Log += "\nLibrary : " + Library;
            foreach (string Src in TestSource)
                Log += "\nSource : " + Src;
            Log += "\nTestStatus : " + (TestStatus?"PASS":"FAIL");
            Log += "\n\nTestResult : \n" + TestResult;
            Log += "\nLatency : " + duration;

            return Log;
        }
    }

    public class BuildMessage
    {
        public class BuildVersion
        {
            public int Major { get; set; }
            public int Minor { get; set; }
            public int BuildNo { get; set; }
            public int Revision { get; set; }
            public bool AutoIncrement { get; set; }

            public BuildVersion() {
                Major = 0; Minor = 0; BuildNo = 0; Revision = 0;
            }

            public void setBuildVersion(int MJ = 0, int MN = 0, int Build = 0, int Rev = 0)
            {
                Major = MJ; Minor = MN; BuildNo = Build; Revision = Rev;
            }

            public void setBuildVersion(string VersionString)
            {
                string[] numStr = VersionString.Split('.');
                Major = int.Parse(numStr[0]);
                Minor = int.Parse(numStr[1]);
                BuildNo = int.Parse(numStr[2]);
                Revision = int.Parse(numStr[3]);
            }

            public override string ToString()
            {
                StringBuilder sb = new StringBuilder();
                sb.AppendFormat("{0}.{1}.{2}.{3}", Major, Minor, BuildNo, Revision);
                return sb.ToString();
            }
        };

        public int Version { get; set; }
        public string Author { get; set; }
        public DateTime TimeStamp { get; set; }
        public TimeSpan duration { get; set; }
        public string refCommitId { get; set; }
        public BuildVersion bVersion;
        public string Name { get; set; }
        public string SourceFile { get; set; }
        public List<string> References { get; set; }
        public List<string> Sources { get; set; }
        public List<string> Libraries { get; set; }
        public bool BuildStatus { get; set; }
        public string BuildResult { get; set; }
        //string DateTimeFormat = "yyMMdd-HHmmss-fff";

        public BuildMessage()
        {
            TimeStamp = DateTime.Now;
            References = new List<string>();
            Sources = new List<string>();
            bVersion = new BuildVersion();
            Libraries = new List<string>();
        }

        public BuildMessage(string name)
        {
            Name = name;
            TimeStamp = DateTime.Now;
            References = new List<string>();
            Sources = new List<string>();
            bVersion = new BuildVersion();
            Libraries = new List<string>();
        }

        public void addSourceFile(string name)
        {
            SourceFile = name;
        }

        public void addReferences(string name)
        {
            References.Add(name);
        }

        public void addSources(string name)
        {
            Sources.Add(name);
        }

        public string GetBuildRequestXML()
        {
            string BuildRequest;

            BuildRequest = "<?xml version=\"1.0\" encoding=\"utf-8\" ?>";
            BuildRequest += "<BuildRequest>";
            BuildRequest += "<Version>1</Version>";
            BuildRequest += "<Author>" + Author + "</Author>";
            BuildRequest += "<TimeStamp>" + TimeStamp + "</TimeStamp>";
            BuildRequest += "<CommitId>" + refCommitId + "</CommitId>";
            BuildRequest += "<Build Name=\"" + Name + "\">";
            BuildRequest += "<BuildVersion>" + bVersion.ToString() + "</BuildVersion>";
            if (SourceFile != null)
                BuildRequest += "  <Source>" + Path.GetFileName(SourceFile) + "</Source>";
            foreach (string Ref in References)
                BuildRequest += "  <Ref>" + Path.GetFileName(Ref) + "</Ref>";
            foreach (string src in Sources)
                BuildRequest += "  <Src>" + Path.GetFileName(src) + "</Src>";
            BuildRequest += "</Build>";
            BuildRequest += "</BuildRequest>";

            return BuildRequest;
        }

        public void ParseBuildRequestXML(string BuildRequestXmlString)
        {
            //call XML parser to parse string
            XmlBuilderParser Parser = new XmlBuilderParser();
            Parser.ParseXmlString(BuildRequestXmlString);

            //fill Build message structure with parsed info
            Version = Parser._xmlInfoList[0]._Version;
            Author = Parser._xmlInfoList[0]._Author;
            TimeStamp = Parser._xmlInfoList[0]._TimeStamp;
            refCommitId = Parser._xmlInfoList[0]._refCommitId;
            Name = Parser._xmlInfoList[0]._Name;
            bVersion.setBuildVersion(Parser._xmlInfoList[0]._bVer);
            SourceFile = Parser._xmlInfoList[0]._Src;
            foreach (var Ref in Parser._xmlInfoList[0]._Ref)
            {
                References.Add(Ref);
            }
            foreach (var Src in Parser._xmlInfoList[0]._Srcs)
            {
                Sources.Add(Src);
            }
        }

        public string GetBuildResultXML()
        {
            string BuildResultXml;

            BuildResultXml = "<?xml version=\"1.0\" encoding=\"utf-8\" ?>";
            BuildResultXml += "<BuildResult>";
            BuildResultXml += "<Author>" + Author + "</Author>";
            BuildResultXml += "<TimeStamp>" + TimeStamp + "</TimeStamp>";
            BuildResultXml += "<CommitId>" + refCommitId+ "</CommitId>";
            BuildResultXml += "<Build Name=\"" + Name + "\">";
            BuildResultXml += "<BuildVersion>" + bVersion.ToString() + "</BuildVersion>";
            if (SourceFile != null)
                BuildResultXml += "  <Source>" + Path.GetFileName(SourceFile) + "</Source>";
            foreach (string Ref in References)
            {
                BuildResultXml += "  <Ref>" + Path.GetFileName(Ref) + "</Ref>";
            }
            foreach (string Src in Sources)
            {
                BuildResultXml += "  <Src>" + Path.GetFileName(Src) + "</Src>";
            }
            foreach (string Lib in Libraries)
            {
                BuildResultXml += "  <Lib>" + Path.GetFileName(Lib) + "</Lib>";
            }
            BuildResultXml += "<BuildStatus>" + BuildStatus.ToString() + "</BuildStatus>";
            BuildResultXml += "<Log>" + BuildResult + "</Log>";
            BuildResultXml += "</Build>";
            BuildResultXml += "</BuildResult>";

            return BuildResultXml;
        }

        public void ParseBuildResultXML(string BuildResultXML)
        {
            XDocument _xDoc = XDocument.Parse(BuildResultXML);
            Author = _xDoc.Descendants("Author").First().Value;
            TimeStamp = DateTime.Parse(_xDoc.Descendants("TimeStamp").First().Value);
            refCommitId = _xDoc.Descendants("CommitId").First().Value;
            XElement[] xElement = _xDoc.Descendants("Build").ToArray();

            foreach (var item in xElement)
            {
                Name = item.Attribute("Name").Value;
                bVersion.setBuildVersion(item.Element("BuildVersion").Value);
                if (item.Element("Source") != null)
                    SourceFile = item.Element("Source").Value;
                IEnumerable<XElement> xBuildRef = item.Elements("Ref");
                foreach (var Ref in xBuildRef)
                {
                    References.Add(Ref.Value);
                }
                IEnumerable<XElement> xBuildSrc = item.Elements("Src");
                foreach (var Src in xBuildSrc)
                {
                    Sources.Add(Src.Value);
                }
                IEnumerable<XElement> xBuildLib = item.Elements("Lib");
                foreach (var Lib in xBuildLib)
                {
                    Libraries.Add(Lib.Value);
                }
                BuildStatus = Convert.ToBoolean(item.Element("BuildStatus").Value);
                BuildResult = item.Element("Log").Value;
            }
        }

        public void DisplayResult()
        {
            Console.WriteLine("  {0,-12} : {1}", "Version", Version);
            Console.WriteLine("  {0,-12} : {1}", "Author", Author);
            Console.WriteLine("  {0,-12} : {1}", "TimeStamp", TimeStamp);
            Console.WriteLine("  {0,-12} : {1}", "CommitId", refCommitId);
            Console.WriteLine("  {0,-12} : {1}", "Latency", duration);
            Console.WriteLine("  {0,-12} : {1}", "BuildName", Name);
            Console.WriteLine("  {0,-12} : {1}", "BuildVersion", bVersion.ToString());
            Console.WriteLine("  {0,-12} : {1}", "Source", SourceFile);
            foreach (string Ref in References)
            {
                Console.WriteLine("  {0,-12} : {1}", "Reference", Ref);
            }
            foreach (string Src in Sources)
            {
                Console.WriteLine("  {0,-12} : {1}", "Sources", Src);
            }
            foreach (string Lib in Libraries)
            {
                Console.WriteLine("  {0,-12} : {1}", "Library", Lib);
            }
            Console.WriteLine("  {0,-12} : {1}", "BuildStatus", (BuildStatus ? "PASS" : "FAIL"));
            Console.WriteLine("-----------Build Result-----------");
            Console.WriteLine("{0}", BuildResult);
            Console.WriteLine("--------------------------------");
            Console.WriteLine("");
        }

        public string GetDetailedBuildResult()
        {
            string Log = null;

            Log += "Author : " + Author;
            Log += "\nTimeStamp : " + TimeStamp;
            Log += "\nCommitId: " + refCommitId;
            Log += "\nBuildName : " + Name;
            Log += "\nBuildVersion : " + bVersion.ToString();
            Log += "\nSource : " + SourceFile;
            foreach (string Ref in References)
                Log += "\nRefernece : " + Ref;
            foreach (string Src in Sources)
                Log += "\nSources : " + Src;
            foreach (string Lib in Libraries)
                Log += "\nLibrary : " + Lib;
            Log += "\nBuildStatus : " + (BuildStatus ? "PASS" : "FAIL");
            Log += "\n\nBuildResult : \n" + BuildResult;
            Log += "\nLatency : " + duration;

            return Log;
        }
    }

    public class RepoMessage
    {
        public int Version { get; set; }
        public string Author { get; set; }
        public DateTime TimeStamp { get; set; }
        public TimeSpan duration { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string CommitId { get; set; }
        public string buildVersion { get; set; }
        public List<string> Sources { get; set; }
        public bool CommitStatus { get; set; }
        public string CommitResult { get; set; }
        string DateTimeFormat = @"MM/dd/yyyy HH:mm:ss:fffffff";

        public RepoMessage()
        {
            TimeStamp = DateTime.Now;
            Sources = new List<string>();
        }

        public RepoMessage(string id)
        {
            Title = id;
            TimeStamp = DateTime.Now;
            Sources = new List<string>();
        }

        public void addSource(string name)
        {
            Sources.Add(name);
        }

        private string getDateTimeString()
        {
            return TimeStamp.ToString(DateTimeFormat);
        }

        public override int GetHashCode()
        {
            return getDateTimeString().GetHashCode();
        }

        public string GetRepoRequestXML()
        {
            string RepoRequest;

            RepoRequest = "<?xml version=\"1.0\" encoding=\"utf-8\" ?>";
            RepoRequest += "<RepoRequest>";
            RepoRequest += "<Version>1</Version>";
            RepoRequest += "<Author>" + Author + "</Author>";
            RepoRequest += "<TimeStamp>" + getDateTimeString() + "</TimeStamp>";
            if (CommitId != null)
                RepoRequest += "<Commit Id=\"" + CommitId + "\">";
            else
                RepoRequest += "<Commit>";
            if (buildVersion != null)
                RepoRequest += "<Build Version=\"" + buildVersion + "\"></Build>";
            RepoRequest += "  <Title>" + Title + "</Title>";
            RepoRequest += "  <Description>" + Description + "</Description>";
            foreach (string src in Sources)
                RepoRequest += "  <Source>" + src + "</Source>";
            RepoRequest += "</Commit>";
            RepoRequest += "</RepoRequest>";

            return RepoRequest;
        }

        public void ParseRepoRequestXML(string RepoRequestXmlString)
        {
            //call XML parser to parse string
            XmlRepoParser Parser = new XmlRepoParser();
            Parser.ParseXmlString(RepoRequestXmlString);

            //fill Repo message structure with parsed info
            Version = Parser._xmlInfoList[0]._Version;
            Author = Parser._xmlInfoList[0]._Author;
            TimeStamp = Parser._xmlInfoList[0]._TimeStamp;
            CommitId = Parser._xmlInfoList[0]._CommitId;
            buildVersion = Parser._xmlInfoList[0]._bVersion;
            Title = Parser._xmlInfoList[0]._Title;
            Description = Parser._xmlInfoList[0]._Description;

            foreach (var Source in Parser._xmlInfoList[0]._Src)
            {
                Sources.Add(Source);
            }
        }

        public string GetRepoResultXML()
        {
            string RepoResultXml;

            RepoResultXml = "<?xml version=\"1.0\" encoding=\"utf-8\" ?>";
            RepoResultXml += "<RepoResult>";
            RepoResultXml += "<Author>" + Author + "</Author>";
            RepoResultXml += "<TimeStamp>" + getDateTimeString() + "</TimeStamp>";
            RepoResultXml += "<Commit Id=\"" + CommitId + "\">";
            if(buildVersion != null)
                RepoResultXml += "<Build Version=\"" + buildVersion + "\"/>";
            RepoResultXml += "  <Title>" + Title + "</Title>";
            RepoResultXml += "  <Description>" + Description + "</Description>";
            foreach (string src in Sources)
            {
                RepoResultXml += "  <Source>" + src + "</Source>";
            }
            RepoResultXml += "<Status>" + CommitStatus.ToString() + "</Status>";
            RepoResultXml += "<Log>" + CommitResult + "</Log>";
            RepoResultXml += "</Commit>";
            RepoResultXml += "</RepoResult>";

            return RepoResultXml;
        }

        public void ParseRepoResultXML(string RepoResultXML)
        {
            XDocument _xDoc = XDocument.Parse(RepoResultXML);
            Author = _xDoc.Descendants("Author").First().Value;
            TimeStamp = DateTime.ParseExact(_xDoc.Descendants("TimeStamp").First().Value, DateTimeFormat, null);
            if (TimeStamp == null) {
                TimeStamp = DateTime.Parse(_xDoc.Descendants("TimeStamp").First().Value);
            }

            XElement[] xElement = _xDoc.Descendants("Commit").ToArray();

            foreach (var item in xElement)
            {
                CommitId = item.Attribute("Id").Value;
                if (item.Element("Build") != null)
                    buildVersion = item.Element("Build").Attribute("Version").Value;
                Title = item.Element("Title").Value;
                Description = item.Element("Description").Value;
                IEnumerable<XElement> xRepoSrc = item.Elements("Source");
                foreach (var src in xRepoSrc)
                {
                    Sources.Add(src.Value);
                }
                CommitStatus = Convert.ToBoolean(item.Element("Status").Value);
                CommitResult = item.Element("Log").Value;
            }
        }

        public void DisplayResult()
        {
            Console.WriteLine("  {0,-12} : {1}", "Version", Version);
            Console.WriteLine("  {0,-12} : {1}", "Author", Author);
            Console.WriteLine("  {0,-12} : {1}", "TimeStamp", TimeStamp);
            Console.WriteLine("  {0,-12} : {1}", "Latency", duration);
            Console.WriteLine("  {0,-12} : {1}", "Commit Id", CommitId);
            Console.WriteLine("  {0,-12} : {1}", "BuildVersion", buildVersion);
            Console.WriteLine("  {0,-12} : {1}", "Title", Title);
            Console.WriteLine("  {0,-12} : {1}", "Description", Description);
            foreach (string Src in Sources)
            {
                Console.WriteLine("  {0,-12} : {1}", "Source", Src);
            }
            Console.WriteLine("  {0,-12} : {1}", "CommitStatus", (CommitStatus ? "PASS" : "FAIL"));
            Console.WriteLine("-----------Commit Result-----------");
            Console.WriteLine("{0}", CommitResult);
            Console.WriteLine("--------------------------------");
            Console.WriteLine("");
        }

        public string GetDetailedRepoResult()
        {
            string Log = null;

            Log += "Author : " + Author;
            Log += "\nTimeStamp : " + TimeStamp;
            Log += "\nCommit ID : " + CommitId;
            Log += "\nBuildVersion: " + buildVersion;
            Log += "\nTitle : " + Title;
            Log += "\nDescription : " + Description;
            foreach (string Src in Sources)
                Log += "\nSource : " + Src;
            Log += "\nCommitStatus : " + (CommitStatus ? "PASS" : "FAIL");
            Log += "\n\nCommitResult : \n" + CommitResult;
            Log += "\nLatency : " + duration;

            return Log;
        }
    }
}
