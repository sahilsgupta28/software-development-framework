﻿/**
 * Sender
 * communication interface to send messages
 * 
 * FileName     : Sender.cs
 * Author       : Sahil Gupta
 * Date         : 14 November 2016 
 * Version      : 1.0
 *
 * Public Interface
 * ----------------
 * Sender()
 *  - Constructor to initialize a new sender
 * void CreateNewSession(string url)
 *  - Initialize URL to which sender will be sending
 * void SendMessage(Message msg)
 *  - Interface to send message
 *  
 * Build Process
 * -------------
 * - Required files:   ITest.cs
 * 
 * Maintenance History
 * -------------------
 * ver 1.0 : 14 November 2016
 *  - first release
 */

using System;
using System.ServiceModel;
using System.Collections.Generic;

namespace Communication
{
    using TestInterface;
    using System.Threading;

    public class Sender
    {
        private Dictionary<string, ICommunication> Session;

        public Sender()
        {
            Console.WriteLine("Starting Sender...");
            Session = new Dictionary<string, ICommunication>();
        }

        ~Sender()
        {
            //close session for every active session
            foreach (KeyValuePair<string, ICommunication> ThisSession in Session)
            {
                ((ICommunicationObject)ThisSession.Value).Close();
            }
            Console.WriteLine("Stopping Sender...");
        }

        public void CreateNewSession(string url)
        {
            //Create a session only if we have not already created one
            if (!Session.ContainsKey(url))
            {
                Console.WriteLine("Creating WCF Sender for URL({0})", url);
                EndpointAddress address = new EndpointAddress(url);
                WSHttpBinding WsBinding = new WSHttpBinding();
                Session.Add(url, ChannelFactory<ICommunication>.CreateChannel(WsBinding, address));
            }
        }

        public void SendMessage(Message msg)
        {
            int trycount = 3;
            do
            {
                try
                {
                    //if we have a session created then only send message
                    if (Session.ContainsKey(msg.ToUrl))
                    {
                        ICommunication ThisSession = Session[msg.ToUrl];
                        ThisSession.SendMessage(msg);
                    }
                    break;
                }
                catch (EndpointNotFoundException)
                {
                    Console.WriteLine("EndpointNotFoundException : Retrying to Send Message");
                    Thread.Sleep(250);
                }
                catch (Exception e)
                {
                    Console.WriteLine("Exception : " + e.ToString());
                }
            } while (trycount-- != 0);
        }

        static void Main(string[] args)
        {
            Sender MySender = new Sender();

            string url = @"http://localhost:4000/TestHarness/ICommunication";
            Console.WriteLine("Connecting to ({0})\n", url);

            Console.WriteLine("Starting Sender");
            MySender.CreateNewSession(url);

            MessageBuilder TestRequestMsg = new MessageBuilder(Message.MessageType.TestRequestMessage);
            string payload = "TestRequestMessage";
            TestRequestMsg.BuildMessage(url, url, "Sahil", payload);

            Console.WriteLine("Sending ({0}) Message with Payload: {1}", TestRequestMsg.GetMessage().Type.ToString(), TestRequestMsg.GetMessage().Payload);
            MySender.SendMessage(TestRequestMsg.GetMessage());

            Console.WriteLine("Stopping Sender");
        }
    }
}
