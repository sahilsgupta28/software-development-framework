﻿/**
 * Blocking Queue
 * Demonstrate threads communicating via Queue
 * 
 * FileName     : FileMgr.cs
 * Author       : Sahil Gupta
 * Source       : Jim Fawcett, CST 2-187, Syracuse University
                  (315) 443-3948, jfawcett@twcny.rr.com
 * Date         : 25 September 2016 
 * Version      : 1.0
 * Language     : C#, VS 2015
 *
 *   Module Operations
 *   -----------------
 *   This package implements a generic blocking queue and demonstrates 
 *   communication between two threads using an instance of the queue. 
 *   If the queue is empty when a reader attempts to deQ an item then the
 *   reader will block until the writing thread enQs an item.  Thus waiting
 *   is efficient.
 * 
 *   NOTE:
 *   This blocking queue is implemented using a Monitor and lock, which is
 *   equivalent to using a condition variable with a lock.
 * 
 *   Public Interface
 *   ----------------
 *   BlockingQueue<string> bQ = new BlockingQueue<string>();
 *   bQ.enQ(msg);
 *   string msg = bQ.deQ();
 *
 *   Build Process
 *   -------------
 *   - Required files:   BlockingQueue.cs
 *   - Compiler command: csc BlockingQueue.cs
 * 
 *   Maintenance History
 *   -------------------
 *   ver 1.0 : 22 October 2013
 *     - first release
 * 
 */

using System;
using System.Collections;
using System.Threading;

namespace nsBlockingQueue
{
    public class BlockingQueue<T>
    {
        private Queue blockingQ;
        object locker_ = new object();

        public BlockingQueue()
        {
            blockingQ = new Queue();
        }

        public void enQ(T msg)
        {
            lock (locker_)  // uses Monitor
            {
                blockingQ.Enqueue(msg);
                Monitor.Pulse(locker_);
            }
        }

        public T deQ()
        {
            T msg = default(T);
            lock (locker_)
            {
                while (this.size() == 0)
                {
                    Monitor.Wait(locker_);
                }
                msg = (T)blockingQ.Dequeue();
            }
            return msg;
        }

        /** size
         *  return number of elements in queue
         */
        public int size()
        {
            int count;
            lock (locker_)
            {
                count = blockingQ.Count;
            }
            return count;
        }

        /** clear
         *  purge elements from queue
         */
        public void clear()
        {
            lock (locker_)
            {
                blockingQ.Clear();
            }
        }
    }

    class TestBlockingQueue
    {
        public static void Main(string[] args)
        {
            Console.Write("\n  Testing Monitor-Based Blocking Queue");
            Console.Write("\n ======================================");

            BlockingQueue<string> q = new BlockingQueue<string>();
            Thread t = new Thread(() =>
            {
                string msg;
                while (true)
                {
                    msg = q.deQ();
                    Console.WriteLine("Child thread received msg : {0}", msg);
                    if (msg == "quit")
                        break;
                }
            });
            t.Start();

            string sendMsg = "sending msg #";
            for (int i = 0; i < 20; ++i)
            {
                string temp = sendMsg + i.ToString();
                Console.WriteLine("Main thread sending msg : {0}", temp);
                q.enQ(temp);
            }
            q.enQ("quit");

            t.Join();
        }
    }
}