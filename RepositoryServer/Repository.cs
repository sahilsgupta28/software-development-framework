﻿/**
 * Repository Server
 * A file store that allows for storing and sending files.
 *      
 * FileName     : Repository.cs
 * Author       : Sahil Gupta
 * Date         : 16 November 2016 
 * Version      : 1.0
 * 
 * Public Interface
 * ----------------
 * Repository(string MyUrl, string LocalRepo)
 *  - create new instance of reposiory listening to requests 
 *    at specified URL with localRepo as path to send/receive files
 * void AddFileHost(string FileHostingUrl)
 *  - Creates new host with URL for streaming files using WCF
 * 
 * Build Process
 * -------------
 * - Required files: ITest.cs, BlockingQueue.cs, FileMgr.cs MessageBuilder.cs, MsgParser.cs, Sender.cs, Receiver.cs FileStreamer.cs
 * 
 * Maintenance History
 * -------------------
 * ver 1.0 : 24 September 2016
 *  - first release
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace RepositoryServer
{
    using Communication;
    using TestInterface;
    using nsBlockingQueue;
    using FileHosting;
    using FileManager;
    using System.IO;
    using XmlManager;

    class RepoStorageInterface
    {
        const string xmlFileName = "RepositoryStore.xml";
        RepoXml xml;

        public RepoStorageInterface(string LocalDir)
        {
            xml = new RepoXml(xmlFileName, LocalDir);
        }

        public bool AddCommit(ref RepoMessage rMsg)
        {
            return xml.AddNewRecord(rMsg.CommitId, rMsg.Author, rMsg.Title, rMsg.Description, rMsg.Sources);
        }

        public bool AddBuildToCommit(ref BuildMessage bMsg)
        {
            return xml.AddBuildToCommit(bMsg.refCommitId, bMsg.bVersion.ToString(), bMsg.Libraries);
        }

        public string getLastCommitId()
        {
            return xml.getLastCommitId();
        }

        public List<string> getAllCommitId()
        {
            return xml.getAllCommitId();
        }

        public List<string> getFilesForCommit(string commitId)
        {
            return xml.getFilesForCommit(commitId);
        }

        public List<string> getLibrariesForCommit(string commitId)
        {
            return xml.getLibrariesForCommit(commitId);
        }

        public string getLatestCommitForFile(string filename)
        {
            return xml.getLatestCommitForFile(filename);
        }
        
        public string getCommitIdForBuild(string buildVersion, string libname)
        {
            return xml.getCommitIdForBuild(buildVersion, libname);
        }
        
    }

    class Repository
    {
        string URL;
        string LocalRepo;
        string HeadDir;
        const string headDirName = "Head";

        Thread tSender;
        Sender MySender;
        BlockingQueue<Message> SendQ;

        Thread tReceiver;
        Receiver MyReceiver;
        BlockingQueue<Message> ReceiveQ;

        FileServiceHost FileHost;
        FileMgr Database;
        RepoStorageInterface store;

        Random rnd;                         //Generate Commit ID

        public Repository(string MyUrl, string LocalRepo)
        {
            Console.WriteLine("Starting Repository Server at URL ({0})", MyUrl);
            this.URL = MyUrl;
            this.LocalRepo = LocalRepo;

            CreateHeadDir();

            int seed = (int)DateTime.Now.Ticks;
            rnd = new Random(seed);

            Database = new FileMgr(LocalRepo);
            store = new RepoStorageInterface(LocalRepo);

            SendQ = new BlockingQueue<Message>();
            MySender = new Sender();
            tSender = new Thread(ThreadSender);
            tSender.Start();

            ReceiveQ = new BlockingQueue<Message>();
            MyReceiver = new Receiver();
            tReceiver = new Thread(ThreadReceiver);
            tReceiver.Start();
        }

        //
        //  Repository Configuration Functions
        // 

        private void CreateHeadDir()
        {
            HeadDir = Path.Combine(LocalRepo, headDirName);

            if (!Directory.Exists(HeadDir))
            {
                Directory.CreateDirectory(HeadDir);
            }

        }

        public void AddFileHost(string FileHostingUrl)
        {
            FileHost = FileServiceHost.NewHost(FileHostingUrl, LocalRepo);
        }

        //
        //  Construct and Send Message
        //

        void ThreadSender()
        {
            while (true)
            {
                Message msg = SendQ.deQ();

                if (msg.Type == Message.MessageType.Quit) {
                    Console.WriteLine("Quitting...");
                    return;
                }

                Console.WriteLine("\nSending WCF Message ({0}) to ({1})", msg.Type.ToString(), msg.ToUrl);
                MySender.CreateNewSession(msg.ToUrl);
                MySender.SendMessage(msg);
            }
        }

        private void SendCommitInitMsg(ref RepoMessage rMsg, string FromUrl, string CommitId)
        {
            rMsg.CommitId = CommitId;

            MessageBuilder AckMsg = new MessageBuilder(Message.MessageType.CommitInitiated);
            AckMsg.BuildMessage(URL, FromUrl, rMsg.Author, rMsg.GetRepoResultXML());
            SendQ.enQ(AckMsg.GetMessage());
        }

        private void SendCommitCompletedMsg(ref RepoMessage rMsg, string FromUrl, bool Status, string Result)
        {
            rMsg.CommitStatus = Status;
            rMsg.CommitResult = Result;

            MessageBuilder AckMsg = new MessageBuilder(Message.MessageType.CommitCompleted);
            AckMsg.BuildMessage(URL, FromUrl, rMsg.Author, rMsg.GetRepoResultXML());
            SendQ.enQ(AckMsg.GetMessage());
        }

        private void SendCheckoutResp(ref RepoMessage rMsg, string FromUrl)
        {
            MessageBuilder Rsp = new MessageBuilder(Message.MessageType.Checkout);
            Rsp.BuildMessage(URL, FromUrl, rMsg.Author, rMsg.GetRepoResultXML());
            SendQ.enQ(Rsp.GetMessage());
        }

        //
        //  Recieve and Process Messages
        //

        void ThreadReceiver()
        {
            Message Msg;
            TestMessage TestMsg;
            BuildMessage bMsg;
            RepoMessage rMsg;

            MyReceiver.Init(URL);
            Console.WriteLine("Waiting for Messages...");

            while (true) {

                //TODO : Delegate Work to another thread
                Msg = MyReceiver.GetMessage();
                Console.WriteLine("PROCESSING START ({0}) from ({1}).", Msg.Type.ToString(), Msg.FromUrl);

                switch (Msg.Type) {

                    case Message.MessageType.RepoInfo:
                        processRepoInfoMsg(ref Msg);
                        break;

                    case Message.MessageType.GetCommitInfo:
                        processRepoGetCommitInfo(ref Msg);
                        break;

                    case Message.MessageType.CommitBegin:
                        rMsg = new RepoMessage();
                        rMsg.ParseRepoRequestXML(Msg.Payload); // parse result from XML in structure
                        processCommitBegin(ref rMsg, Msg.FromUrl);
                        break;

                    case Message.MessageType.CommitEnd:
                        rMsg = new RepoMessage();
                        rMsg.ParseRepoRequestXML(Msg.Payload); // parse result from XML in structure
                        processCommitEnd(ref rMsg, Msg.FromUrl);
                        break;

                    case Message.MessageType.StoreBuildLogMsg:
                        bMsg = new BuildMessage();
                        bMsg.ParseBuildResultXML(Msg.Payload);
                        processBuildResultMsg(ref bMsg);
                        break;

                    case Message.MessageType.StoreTestLogMsg:
                        TestMsg = new TestMessage();
                        TestMsg.ParseTestResultXML(Msg.Payload); // parse result from XML in structure
                        
                        Console.WriteLine("Storing logs recieved from Test Harness.");
                        string filename = Database.WriteLog(TestMsg.Author, TestMsg.TestDriver, TestMsg.TestName, Msg.Payload, TestMsg.TestStatus ? "PASS" : "FAIL"); //write to file
                        if (null != filename) {
                            Console.WriteLine("Storing Test Logs with file name ({0}) including developer id and datetime", filename);
                        }

                        break;

                    case Message.MessageType.GetTestLogMsg:
                        TestMsg = new TestMessage();
                        TestMsg.ParseQueryTestResultXML(Msg.Payload); // parse result from XML in structure
                        
                        Console.WriteLine("Query for Test Log for Author({0}) with Test Name({1})", TestMsg.Author, TestMsg.TestName);
                        string TestResult = Database.GetDriverTestResultString(TestMsg.Author, TestMsg.TestName); //Search for test result
                        if (null == TestResult) {
                            TestResult = TestMsg.GetTestResultXML(false, "Invalid Test Name");
                        }
                    
                        //send message containing result in payload
                        MessageBuilder TestLogMsg = new MessageBuilder(Message.MessageType.TestLogsAck);
                        TestLogMsg.BuildMessage(URL, Msg.FromUrl, Msg.Author, TestResult);
                        SendQ.enQ(TestLogMsg.GetMessage());
                        break;

                    case Message.MessageType.Checkout:
                        rMsg = new RepoMessage();
                        rMsg.ParseRepoRequestXML(Msg.Payload); // parse result from XML in structure
                        checkout(ref rMsg, Msg.FromUrl);
                        break;

                    case Message.MessageType.Quit:
                        SendQ.enQ(Msg);
                        return;
                }
                Console.WriteLine("PROCESSING END ({0})", Msg.Type.ToString());
            }
        }       

        private void processRepoInfoMsg(ref Message Msg)
        {
            string CommitIds = null;

            List<string> commitIds = store.getAllCommitId();
            if(commitIds.Count != 0)
                CommitIds = string.Join(",", commitIds.ToArray());

            Console.WriteLine("Returining {0} commit ID's to client", commitIds.Count);

            // Send Response
            MessageBuilder AckMsg = new MessageBuilder(Msg.Type);
            AckMsg.BuildMessage(URL, Msg.FromUrl, Msg.Author, CommitIds);
            SendQ.enQ(AckMsg.GetMessage());
        }

        private void processRepoGetCommitInfo(ref Message Msg)
        {
            RepoMessage rMsg = new RepoMessage();
            rMsg.ParseRepoRequestXML(Msg.Payload);

            List<string> files = store.getFilesForCommit(rMsg.CommitId);
            foreach (string file in files)
            {
                Console.WriteLine("Checkout : Retrieved file ({0}) for commit ID ({1})", file, rMsg.CommitId);
                rMsg.addSource(file);
            }

            List<string> libs = store.getLibrariesForCommit(rMsg.CommitId);
            foreach (string lib in libs)
            {
                Console.WriteLine("Checkout : Retrieved lib ({0}) for commit ID ({1})", lib, rMsg.CommitId);
                rMsg.addSource(lib);
            }

            // Send Response
            MessageBuilder AckMsg = new MessageBuilder(Msg.Type);
            AckMsg.BuildMessage(URL, Msg.FromUrl, Msg.Author, rMsg.GetRepoResultXML());
            SendQ.enQ(AckMsg.GetMessage());
        }

        private void processCommitBegin(ref RepoMessage rMsg, string SourceUrl)
        {
            string CommitId = getNewComitId();
            Console.WriteLine("Generated New Commit with ID : " + CommitId);

            // Send Response
            SendCommitInitMsg(ref rMsg, SourceUrl, CommitId);
        }

        private void processCommitEnd(ref RepoMessage rMsg, string SourceUrl)
        {
            string result = "";
            //todo
            //Verify Commit Files in repo

            //Add commit details to log
            store.AddCommit(ref rMsg);

            //Copy files to head dir
            foreach (string src in rMsg.Sources)
            {
                string srcfilepath = Path.Combine(LocalRepo, rMsg.CommitId, Path.GetFileName(src));
                string dstfilepath = Path.Combine(LocalRepo, headDirName, Path.GetFileName(src));

                File.Copy(srcfilepath, dstfilepath, true);
                result += "Added file " + Path.GetFileName(src) + " to commit " + rMsg.CommitId + ".\n";
            }

            // Send Response
            SendCommitCompletedMsg(ref rMsg, SourceUrl, true, result);
        }

        private void processBuildResultMsg(ref BuildMessage bMsg)
        {
            if (bMsg.BuildStatus == true)
            {
                if(store.AddBuildToCommit(ref bMsg))
                    Console.WriteLine("Added build version ({0}) to commit({1})", bMsg.bVersion.ToString(), bMsg.refCommitId);
            }
        }

        private void checkout(ref RepoMessage rMsg, string SourceUrl)
        {
            rMsg.CommitStatus = true;

            if (rMsg.Sources.Count == 0)
            {
                // USECASE-1 : Checkout all files for commit. (YES-CommitID, NO-Sources, YES/NO-Build)
                if (rMsg.CommitId != null)
                {
                    //Get list of files from XML
                    List<string> files = store.getFilesForCommit(rMsg.CommitId);
                    foreach (string file in files)
                    {
                        Console.WriteLine("Checkout : Retrieved file ({0}) for commit ID ({1})", file, rMsg.CommitId);
                        rMsg.addSource(file);
                    }
                }
            }
            else {
                if (rMsg.CommitId == null) 
                {
                    // USECASE-2 : Checkout library for given build. (NO-CommitID, YES-Sources(Lib), YES-Build)
                    if (rMsg.buildVersion != null) {
                        rMsg.CommitId = store.getCommitIdForBuild(rMsg.buildVersion, rMsg.Sources.First());
                        if (rMsg.CommitId == null) {
                            rMsg.CommitResult = "commit not found in repository for build " + rMsg.buildVersion + ".\n";
                            rMsg.CommitStatus = false;
                        } else {
                            Console.WriteLine("Checkout : Retrieved commit ID ({0}) for ({1},{2})", rMsg.CommitId, rMsg.buildVersion, rMsg.Sources.First());
                        }
                    } else {
                        // USECASE-3 : Checkout latest file. (NO-CommitID, YES-Sources(src), NO-Build)
                        rMsg.CommitId = store.getLatestCommitForFile(rMsg.Sources.First());
                        Console.WriteLine("Checkout : Retrieved commit ID ({0}) for file {1}", rMsg.CommitId, rMsg.Sources.First());
                    }
                }
                else
                {
                    // USECASE-4 : Checkout src/lib for commit. (YES-CommitID, YES-Sources(src/lib), YES/NO-Build)
                    List<string> files = store.getFilesForCommit(rMsg.CommitId);
                    List<string> libs = store.getLibrariesForCommit(rMsg.CommitId);
                    foreach (var item in rMsg.Sources)
                    {
                        if (!files.Contains(item) && !libs.Contains(item))
                        {
                            rMsg.Sources.Remove(item);
                            rMsg.CommitResult = "File " + item + " not found in repository for commit ID " + rMsg.CommitId + ".\n";
                            rMsg.CommitStatus = false;
                            break;
                        }
                    }
                }
            }
            
            //Send reply
            SendCheckoutResp(ref rMsg, SourceUrl);
        }

        //
        //  Communication Helper functions
        //

        private string getNewComitId()
        {
            string id = store.getLastCommitId();
            if(id == null) {
                id = "999";
                //id = rnd.Next().ToString();
            }

            return (int.Parse(id) + 1).ToString();
        }

        static void Main(string[] args)
        {
            string URL = @"http://localhost:4000/ICommunication/Repository";
            string FileHostingUrl = @"http://localhost:4000/IStream/FileHosting";
            string RepositoryPath = "Repository\\RepositoryServerDir";

            Console.Title = "Repository (" + URL + ")";

            Repository MyRepo = new Repository(URL, RepositoryPath);

            MyRepo.AddFileHost(FileHostingUrl);
        }
    }
}
