﻿/**
 * GUI
 * A GUI that allows to build and send test request and see test result
 *      
 * FileName     : GUI.xaml.cs
 * Author       : Sahil Gupta
 * Date         : 16 November 2016 
 * Version      : 1.0
 * 
 * Public Interface
 * ----------------
 * GUI()
 *  - Create New GUI Instance
 *   
 * Build Process
 * -------------
 * - Required files: ITest.cs, client.cs, MessageBuilder.cs
 * 
 * Maintenance History
 * -------------------
 * ver 1.0 : 16 November 2016
 *  - first release
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Threading;
using System.IO;

using nsClient;
using TestInterface;
using Communication;

namespace ClientGUI
{
    using WForms = System.Windows.Forms;
    public partial class GUI : Window
    {
        Client MyClient;
        int MaxResultCount;
        static bool Demo;
        List<string> cache_commit_id;

        public GUI()
        {
            InitializeComponent();
            MaxResultCount = 20;
            string MyUrl;
            cache_commit_id = new List<string>();
            string[] args = Environment.GetCommandLineArgs();

            //For multiple client GUI instance use different URL
            if (1 == args.Length) {
                MyUrl = @"http://localhost:4000/ICommunication/GUI";
            } else {
                MyUrl = args[1];
            }

            Console.Title = "Client GUI Console (" + MyUrl + ")";
            this.Title = "Client GUI Window (" + MyUrl + ")";

            string TestHarnessUrl = @"http://localhost:4000/ICommunication/TestHarness";
            string RepoUrl = @"http://localhost:4000/ICommunication/Repository";
            string BuildSrv = @"http://localhost:4000/ICommunication/BuildServer";
            string FileHostingUrl = @"http://localhost:4000/IStream/FileHosting";

            try {
                Console.WriteLine("Starting GUI Client at URL ({0})", MyUrl);
                MyClient = new Client(MyUrl);
                MyClient.ConfigureURL(TestHarnessUrl, RepoUrl, BuildSrv, FileHostingUrl);
                MyClient.GetAllCommitId();
                if (Demo == false)
                {
                    Demo = true;
                    DemoBasicRequirements();
                }

            } catch (Exception Ex) {
                Console.WriteLine("Exception : {0}", Ex.Message);
            }
        }

        private void DemoBasicRequirements()
        {
            //
            //  Demo 1: Sample Code
            //

            // Upload Files to Repository
            RepoMessage demoCommitReq = new RepoMessage("SampleCodeCommit");
            demoCommitReq.Description = "First commit for sample code and driver";
            demoCommitReq.Author = "Sahil Gupta";
            demoCommitReq.addSource(@"SampleCode\SampleCode.cs");
            demoCommitReq.addSource(@"SampleDriver\SampleDriver.cs");
            demoCommitReq.addSource(@"TestInterface\ITest.cs");
            MyClient.SendCommitRequest(ref demoCommitReq);
            Thread.Sleep(500);  

            // Send Build Request
            BuildMessage demoBuildReq = new BuildMessage("SampleCodeBuild");
            demoBuildReq.Author = "Sahil Gupta";
            demoBuildReq.refCommitId = "1000";
            demoBuildReq.bVersion.setBuildVersion(0, 0, 1000, 0);
            demoBuildReq.addSources(@"SampleCode.cs");
            demoBuildReq.addSources(@"SampleDriver.cs");
            demoBuildReq.addReferences(@"ITest.cs");
            MyClient.SendBuildRequest(demoBuildReq);
            Thread.Sleep(1000);

            // Send Test Request
            TestMessage demoTestReq = new TestMessage("SampleCodeTest");
            demoTestReq.Author = "Sahil Gupta";
            demoTestReq.refCommitId = "1000";
            demoTestReq.addDriver(@"SampleDriver.dll");
            demoTestReq.addCode(@"SampleCode.dll");
            MyClient.SendTestRequest(demoTestReq);
            Thread.Sleep(500);

            // Send Test Result Query
            TestMessage demoQueryTestReq = new TestMessage("SampleCodeTest");
            demoQueryTestReq.Author = "Sahil Gupta";
            MyClient.GetTestResult(demoQueryTestReq);

            // Checkout Files
            RepoMessage ckout = new RepoMessage();
            ckout.Author = "Sahil Gupta";
            ckout.CommitId = "1000";
            MyClient.checkout(ref ckout, "Repository\\Checkout\\SampleCodePrj");

            // 
            //  Demo 2: Exception Test Driver
            //
            RepoMessage demoCommitReq2 = new RepoMessage("ExceptionCodeCommit");
            demoCommitReq2.Description = "First commit for sample exception code and driver";
            demoCommitReq2.Author = "Sahil Gupta";
            demoCommitReq2.addSource(@"SampleCode\SampleCode.cs");
            demoCommitReq2.addSource(@"ExceptionTestDriver\ExceptionTD.cs");
            demoCommitReq2.addSource(@"TestInterface\ITest.cs");
            MyClient.SendCommitRequest(ref demoCommitReq2);
            Thread.Sleep(500);

            BuildMessage demoBuildReq2 = new BuildMessage("ExceptionCodeBuild");
            demoBuildReq2.Author = "Sahil Gupta";
            demoBuildReq2.refCommitId = "1001";
            demoBuildReq2.bVersion.setBuildVersion("0.0.1001.0");
            demoBuildReq2.addSources(@"SampleCode.cs");
            demoBuildReq2.addSources(@"ExceptionTD.cs");
            demoBuildReq2.addReferences(@"ITest.cs");
            MyClient.SendBuildRequest(demoBuildReq2);
            Thread.Sleep(1000);

            TestMessage demoTestReq2 = new TestMessage("ExceptionCodeTest");
            demoTestReq2.Author = "Sahil Gupta";
            demoTestReq2.refCommitId = "1001";
            demoTestReq2.addDriver(@"ExceptionTD.dll");
            demoTestReq2.addCode(@"SampleCode.dll");
            MyClient.SendTestRequest(demoTestReq2);
            Thread.Sleep(500);

            TestMessage demoQueryTestReq2 = new TestMessage("ExceptionCodeTest");
            demoQueryTestReq2.Author = "Sahil Gupta";
            MyClient.GetTestResult(demoQueryTestReq2);

            RepoMessage ckout2 = new RepoMessage();
            ckout2.Author = "Sahil Gupta";
            ckout2.CommitId = "1001";
            ckout2.addSource("SampleCode.cs");
            MyClient.checkout(ref ckout2, "Repository\\Checkout\\ExceptionPrj");

            //
            //  Demo 3: Dependency Analysis
            //

            RepoMessage demoCommitNewReq = new RepoMessage("DepAnalCommit");
            demoCommitNewReq.Description = "Dependency Analysis Source and Test Driver";
            demoCommitNewReq.Author = "Sahil Gupta";
            demoCommitNewReq.addSource(@"DaTestDriver\DaTestDriver.cs");
            demoCommitNewReq.addSource(@"DepAnal\DepAnal.cs");
            demoCommitNewReq.addSource(@"TestInterface\ITest.cs");
            MyClient.SendCommitRequest(ref demoCommitNewReq);
            Thread.Sleep(500);

            BuildMessage demoBuildReq3 = new BuildMessage("DepAnalBuild");
            demoBuildReq3.Author = "Sahil Gupta";
            demoBuildReq3.refCommitId = "1002";
            demoBuildReq3.addSources(@"DaTestDriver.cs");
            demoBuildReq3.addSources(@"DepAnal.cs");
            demoBuildReq3.addReferences(@"ITest.cs");
            MyClient.SendBuildRequest(demoBuildReq3);
            Thread.Sleep(1000);

            TestMessage demoTestReq3 = new TestMessage("DepAnalTest");
            demoTestReq3.Author = "Sahil Gupta";
            demoTestReq3.refCommitId = "1002";
            demoTestReq3.addDriver(@"DaTestDriver.dll");
            demoTestReq3.addCode(@"DepAnal.dll");
            MyClient.SendTestRequest(demoTestReq3);
            Thread.Sleep(500);

            TestMessage demoQueryTestReq3 = new TestMessage("DepAnalTest");
            demoQueryTestReq3.Author = "Sahil Gupta";
            MyClient.GetTestResult(demoQueryTestReq3);

            RepoMessage ckout3 = new RepoMessage();
            ckout3.Author = "Sahil Gupta";
            ckout3.CommitId = "1002";
            ckout3.addSource("DaTestDriver.dll");
            MyClient.checkout(ref ckout3, "Repository\\Checkout\\DepAnalPrj");
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            //Start thread to accept result of requests sent by SDF Client
            Thread t = new Thread(ProcessSDFAcrivity);
            t.Start();
        }

        private void ProcessSDFAcrivity()
        {
            bool UpdateActivityLog;
            ClientResult clientResult = null;

            while (true)
            {
                //wait for message result
                Message Msg = MyClient.ProcessClientResult();
                UpdateActivityLog = true;

                switch (Msg.Type)
                {
                    case Message.MessageType.RepoInfo:
                        Dispatcher.Invoke(new Action<string>(processRepoInfo), System.Windows.Threading.DispatcherPriority.Normal, Msg.Payload);
                        UpdateActivityLog = false;
                        break;

                    case Message.MessageType.GetCommitInfo:
                        Dispatcher.Invoke(new Action<string>(processCommitInfo), System.Windows.Threading.DispatcherPriority.Normal, Msg.Payload);
                        UpdateActivityLog = false;
                        break;

                    case Message.MessageType.CommitCompleted:
                        clientResult = processCheckInResponse(ref Msg);
                        break;

                    case Message.MessageType.Checkout:
                        clientResult = processCheckOutResponse(ref Msg);
                        break;

                    case Message.MessageType.BuildAck:
                        clientResult = processBuildResponse(ref Msg);
                        break;

                    case Message.MessageType.TestRequestAck:
                    case Message.MessageType.TestLogsAck:
                        clientResult = ProcessTestResponse(ref Msg);
                        break;

                    case Message.MessageType.Quit:
                        return;

                    default:
                        Console.WriteLine("Unknown Message ({0}) Received:", Msg.Type.ToString());
                        UpdateActivityLog = false;
                        break;
                }

                //update listbox on GUI with test result
                if (UpdateActivityLog)
                {
                    Dispatcher.Invoke(
                        new Action<ClientResult>(UpdateActivityWindow),
                        System.Windows.Threading.DispatcherPriority.Background,
                        clientResult);
                }
            }
        }

        private void processRepoInfo(string cids)
        {
            cache_commit_id.Clear();
            cmb_FC.Items.Clear();
            cbx_TCID.Items.Clear();
            cmb_b_commit.Items.Clear();

            if (null == cids)
                return;

            string[] cid = cids.Split(',');
            foreach (var commitID in cid)
	        {
                NewCommitActions(commitID);
	        }
        }

        private void NewCommitActions(string commitID)
        {
            cache_commit_id.Add(commitID);

            cmb_FC.Items.Add(commitID);
            cmb_FC.SelectedIndex = cmb_b_commit.Items.Count;

            cmb_b_commit.Items.Add(commitID);
            cmb_b_commit.SelectedIndex = cmb_b_commit.Items.Count;

            cbx_TCID.Items.Add(commitID);
            cbx_TCID.SelectedIndex = cbx_TCID.Items.Count;
        }

        private void processCommitInfo(string Payload)
        {
            RepoMessage rMsg = new RepoMessage();
            rMsg.ParseRepoResultXML(Payload);

            if (rMsg.CommitId == (string)cmb_b_commit.SelectedValue)
            {
                lbx_b_src.Items.Clear();
                lbx_b_ref.Items.Clear();
                foreach (var item in rMsg.Sources)
                {
                    if ("cs" != item.Split('.').Last()) {
                        continue;
                    }
                    lbx_b_src.Items.Add(item);
                    lbx_b_ref.Items.Add(item);
                }
            }
            if (rMsg.CommitId == (string)cbx_TCID.SelectedValue)
            {
                cmb_TD.Items.Clear();
                cmb_TC.Items.Clear();
                foreach (var item in rMsg.Sources)
                {
                    if ("dll" != item.Split('.').Last()) {
                        continue;
                    }
                    cmb_TD.Items.Add(item);
                    cmb_TC.Items.Add(item);
                }
                cmb_TD.SelectedIndex = 0;
                cmb_TC.SelectedIndex = 0;
            }
        }

        private ClientResult processCheckInResponse(ref Message Msg)
        {
            RepoMessage rMsg;
            rMsg = new RepoMessage();
            rMsg.ParseRepoResultXML(Msg.Payload);

            ClientResult commitResult = new ClientResult();
            commitResult.RequestName = rMsg.Title;
            commitResult.DisplayLog = "Check-In:: " + rMsg.Title + " :: " + (rMsg.CommitStatus ? "PASS" : "FAIL") + " :: " + rMsg.Author;
            commitResult.DetailedLog = rMsg.CommitResult;

            if (rMsg.CommitStatus == true) {
                Dispatcher.Invoke(new Action<string>(NewCommitActions), System.Windows.Threading.DispatcherPriority.Background, rMsg.CommitId);
            }

            Console.WriteLine("Updating Checkin Result : ({0})", commitResult.DisplayLog);

            return commitResult;
        }

        private ClientResult processCheckOutResponse(ref Message Msg)
        {
            RepoMessage rMsg;
            rMsg = new RepoMessage();
            rMsg.ParseRepoResultXML(Msg.Payload);

            ClientResult commitResult = new ClientResult();
            commitResult.RequestName = rMsg.Title == null ? "Checkout Details" : rMsg.Title;
            commitResult.DisplayLog = "Check-Out :: "  + (rMsg.CommitStatus ? "PASS" : "FAIL") + " :: " + rMsg.Author;

            if (rMsg.CommitStatus == true)
            {
                foreach (string file in rMsg.Sources)
                {
                    commitResult.DetailedLog += "Succeeded checkout file " + file + " from Repository for commit " + rMsg.CommitId + ".\n";
                }
            }
            
            Console.WriteLine("Updating Checkin Result : ({0})", commitResult.DisplayLog);

            return commitResult;
        }

        private ClientResult processBuildResponse(ref Message Msg)
        {
            BuildMessage bMsg;
            bMsg = new BuildMessage();
            bMsg.ParseBuildResultXML(Msg.Payload);

            ClientResult buildResult = new ClientResult();
            buildResult.RequestName = bMsg.Name;
            buildResult.DisplayLog = "BuildResult :: " + bMsg.Name + " :: " + (bMsg.BuildStatus ? "PASS" : "FAIL") + " :: " + bMsg.Author;
            buildResult.DetailedLog = bMsg.BuildResult;

            Console.WriteLine("Updating Build Result : ({0})", buildResult.DisplayLog);

            return buildResult;
        }

        private ClientResult ProcessTestResponse(ref Message Msg)
        {
            TestMessage tMsg;
            tMsg = new TestMessage();
            tMsg.ParseTestResultXML(Msg.Payload);
            
            tMsg.duration = DateTime.Now.Subtract(Msg.TimeStamp);

            ClientResult testResult = new ClientResult();
            testResult.RequestName = tMsg.TestName;
            
            string testtype = "";
            if (Msg.Type == Message.MessageType.TestRequestAck) testtype = "TestResult :: ";
            if (Msg.Type == Message.MessageType.TestLogsAck) testtype = "QueryResult :: ";
            testResult.DisplayLog = testtype + tMsg.TestName + " :: " + (tMsg.TestStatus ? "PASS" : "FAIL") + " :: " + tMsg.Author;
            
            testResult.DetailedLog = tMsg.GetDetailedTestResult();

            Console.WriteLine("Updating Test Result Summary : ({0})", testResult.DisplayLog);

            return testResult;
        }

        private void UpdateActivityWindow(ClientResult clientResult)
        {
            //delete last item from list box if count for max items to display is reached
            if (lbx_activity.Items.Count == MaxResultCount)
            {
                lbx_activity.Items.RemoveAt(MaxResultCount - 1);
            }

            //add new items to top of list box
            lbx_activity.Items.Insert(0, clientResult);
        }

        private void lbx_Activity_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //get selected item
            var addedItems = e.AddedItems;
            if (addedItems.Count > 0)
            {
                //display details for selected test result
                ClientResult selectedItem = addedItems[0] as ClientResult;
                string TitleCaption = selectedItem.RequestName;
                MessageBox.Show(selectedItem.DetailedLog, TitleCaption);
            }
            //de-select that item so it can be selected again
            lbx_activity.UnselectAll();
        }

        //
        // Repository Interaction
        //
        private void radio_FileCheckIn_Checked(object sender, RoutedEventArgs e)
        {
            lbl_FD.Visibility = Visibility.Visible;
            tbx_FD.Visibility = Visibility.Visible;

            btn_FFiles.Visibility = Visibility.Visible;

            lbl_FC.Visibility = Visibility.Hidden;
            cmb_FC.Visibility = Visibility.Hidden;

            lbl_FV.Visibility = Visibility.Hidden;
            tbx_FV.Visibility = Visibility.Hidden;
            
            lbl_FLoc.Visibility = Visibility.Hidden;
            tbx_FLoc.Visibility = Visibility.Hidden;
            btn_FLoc.Visibility = Visibility.Hidden;

            btn_FSubmit.Content = "check-in";
        }

        private void radio_FileCheckOut_Checked(object sender, RoutedEventArgs e)
        {
            //this is the default action and the GUI is not initialized yet so return
            if (null == lbl_FD) return;

            lbl_FD.Visibility = Visibility.Hidden;
            tbx_FD.Visibility = Visibility.Hidden;

            btn_FFiles.Visibility = Visibility.Hidden;

            lbl_FC.Visibility = Visibility.Visible;
            cmb_FC.Visibility = Visibility.Visible;

            lbl_FV.Visibility = Visibility.Visible;
            tbx_FV.Visibility = Visibility.Visible;

            lbl_FLoc.Visibility = Visibility.Visible;
            tbx_FLoc.Visibility = Visibility.Visible;
            btn_FLoc.Visibility = Visibility.Visible;

            btn_FSubmit.Content = "check-out";
        }

        private void submitFile_Click(object sender, RoutedEventArgs e)
        {
            //validate author name and test name should not be null
            if (tbx_FA.Text == "")
            {
                MessageBox.Show("Enter Author Name", "Error");
                return;
            }
            if (tbx_FT.Text == "")
            {
                MessageBox.Show("Enter Commit Title", "Error");
                return;
            }

            RepoMessage NewRepoRequest = new RepoMessage(tbx_FT.Text);
            NewRepoRequest.Author = tbx_FA.Text;

            if (true == rad_File_CheckIn.IsChecked)
            {
                /* Parse Description */
                NewRepoRequest.Description = tbx_FD.Text;

                /* Parse Test Code Files */
                if (tbx_FFiles.Text != "")
                {
                    string[] TestCodes = tbx_FFiles.Text.Split(',');
                    //for multiple test codes, split and add each file
                    foreach (var TestCode in TestCodes)
                    {
                        string Code = TestCode.Trim(new Char[] { '\"' });
                        if (!File.Exists(Code))
                        {
                            MessageBox.Show("Invalid File Path : " + Code, "Error");
                            return;
                        }
                        NewRepoRequest.addSource(Code);
                    }
                }

                Console.WriteLine("\nSending New Commit Request");
                MyClient.SendCommitRequest(ref NewRepoRequest);
            }
            else
            { /* true == rad_File_CheckOut.IsChecked */

                if (cmb_FC.SelectedIndex >= 0)
                    NewRepoRequest.CommitId = (string)cmb_FC.SelectedValue;          /* Parse Commit ID */
                if (tbx_FV.Text != "")
                    NewRepoRequest.buildVersion = tbx_FV.Text;      /* Parse Build Version ID */

                if (tbx_FFiles.Text != "")
                {
                    string[] TestCodes = tbx_FFiles.Text.Split(',');
                    foreach (var TestCode in TestCodes)
                    {
                        string Code = TestCode.Trim(new Char[] { '\"' });
                        NewRepoRequest.addSource(Code);
                    }
                }

                Console.WriteLine("\nSending New Test Query");
                MyClient.checkout(ref NewRepoRequest, tbx_FLoc.Text);
            }
        }

        private void button_FFiles_Click(object sender, RoutedEventArgs e)
        {
            // Create OpenFileDialog 
            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();

            // Set filter for file extension and default file extension 
            dlg.DefaultExt = ".cs";
            dlg.Filter = "CS Files (*.cs)|*.cs";
            dlg.Title = "File Browser";
            dlg.Multiselect = true;

            // Display OpenFileDialog by calling ShowDialog method 
            Nullable<bool> result = dlg.ShowDialog();

            // Get the selected file name and display in a TextBox 
            if (result == true)
            {
                tbx_FFiles.Text = "";
                foreach (String file in dlg.FileNames)
                    tbx_FFiles.Text += "\"" + file + "\",";
                tbx_FFiles.Text = tbx_FFiles.Text.TrimEnd(',');
                Console.WriteLine("Check Files : {0}", tbx_FFiles.Text);
            }
        }

        private void button_FLoc_Click(object sender, RoutedEventArgs e)
        {
            // Create OpenFileDialog 
            WForms.FolderBrowserDialog dlg = new WForms.FolderBrowserDialog();

            // Set filter for file extension and default file extension 
            dlg.Description = "Select directory to check-out files";

            // Display OpenFileDialog by calling ShowDialog method 
            // Get the selected file name and display in a TextBox 
            if (dlg.ShowDialog() == WForms.DialogResult.OK)
            {
                tbx_FLoc.Text = dlg.SelectedPath;
                Console.WriteLine("Checkout Location : {0}", tbx_FLoc.Text);
            }
        }

        //
        // Build Server Interaction
        //
        private void lbx_b_src_GotFocus(object sender, RoutedEventArgs e)
        {
            //lbx_b_src.Height = lbx_b_src.Items.Count * 25;
            //lbx_b_src.UpdateLayout();
            //lbx_b_src.BringIntoView();
        }

        private void lbx_b_src_LostFocus(object sender, RoutedEventArgs e)
        {
            //lbx_b_src.Height = 25;
        }

        private void cmb_b_commit_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (cmb_b_commit.SelectedIndex == -1)
            {
                return;
            }

            RepoMessage rMsg = new RepoMessage();
            rMsg.CommitId = (string)cmb_b_commit.SelectedValue;
            MyClient.GetCommitInfo(ref rMsg);
        }

        private void submitBuild_Click(object sender, RoutedEventArgs e)
        {
            //validate author name and build title should not be null
            if (tbx_BA.Text == "")
            {
                MessageBox.Show("Enter Author Name", "Error");
                return;
            }
            if (tbx_BN.Text == "")
            {
                MessageBox.Show("Enter Build Title", "Error");
                return;
            }

            BuildMessage demoBuildCodeReq = new BuildMessage(tbx_BN.Text);
            demoBuildCodeReq.Author = tbx_BA.Text;
            demoBuildCodeReq.refCommitId = (string)cmb_b_commit.SelectedValue;
            demoBuildCodeReq.bVersion.setBuildVersion(tbx_BV.Text);

            /* Parse Source Files */
            foreach (var item in lbx_b_src.SelectedItems)
            {
                demoBuildCodeReq.addSources(item.ToString());
            }
        
            /* Parse Reference Files */
            foreach (var item in lbx_b_ref.SelectedItems)
            {
                demoBuildCodeReq.addReferences(item.ToString());
            }

            Console.WriteLine("\nSending New Build Request");
            MyClient.SendBuildRequest(demoBuildCodeReq);
        }

        //
        // Test Harness Interaction
        //

        private void radio_TestCode_Checked(object sender, RoutedEventArgs e)
        {
            //this is the default action and the GUI is not initialized yet so return
            if (null == lbl_TCID) return;

            lbl_TCID.Visibility = Visibility.Visible;
            cbx_TCID.Visibility = Visibility.Visible;

            lbl_TD.Visibility = Visibility.Visible;
            cmb_TD.Visibility = Visibility.Visible;

            lbl_TC.Visibility = Visibility.Visible;
            cmb_TC.Visibility = Visibility.Visible;

            btn_Tsubmit.Content = "Submit Test";
        }

        private void radio_GetTestResult_Checked(object sender, RoutedEventArgs e)
        {
            lbl_TCID.Visibility = Visibility.Hidden;
            cbx_TCID.Visibility = Visibility.Hidden;

            lbl_TD.Visibility = Visibility.Hidden;
            cmb_TD.Visibility = Visibility.Hidden;

            lbl_TC.Visibility = Visibility.Hidden;
            cmb_TC.Visibility = Visibility.Hidden;

            btn_Tsubmit.Content = "Get Result";
        }

        private void submitTest_Click(object sender, RoutedEventArgs e)
        {
            //validate author name and test name should not be null
            if (tbx_TA.Text == "") {
                MessageBox.Show("Enter Author Name", "Error");
                return;
            }
            if (tbx_TN.Text == "") {
                MessageBox.Show("Enter Test Name", "Error");
                return;
            }

            TestMessage NewTestRequest = new TestMessage(tbx_TN.Text);
            NewTestRequest.Author = tbx_TA.Text;

            if (true == rad_TC.IsChecked) {

                /* Parse Commit ID */
                NewTestRequest.refCommitId = cbx_TCID.Text;

                NewTestRequest.addDriver((string)cmb_TD.SelectedValue);

                NewTestRequest.addCode((string)cmb_TC.SelectedValue);

                Console.WriteLine("\nSending New Test Request");
                MyClient.SendTestRequest(NewTestRequest);
            }
            else { /* true == rad_TR.IsChecked */
                Console.WriteLine("\nSending New Test Query");
                MyClient.GetTestResult(NewTestRequest);
            }
        }

        private void cbx_TCID_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (cbx_TCID.SelectedIndex == -1)
            {
                return;
            }

            RepoMessage rMsg = new RepoMessage();
            rMsg.CommitId = (string)cbx_TCID.SelectedValue;
            MyClient.GetCommitInfo(ref rMsg);
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            MyClient.Quit();
        }

        private void cmb_FC_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }
    }

    public class ClientResult
    {
        public string RequestName { get; set; }
        public string DisplayLog { get; set; }
        public string DetailedLog { get; set; }
    }
}
