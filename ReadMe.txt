Software Development Framework

Building Solution:

 - Use the visual studio command prompt to build (Run Visual Studio->Tools->Visual Studio Command Prompt)
 - Navigate to root folder (containing solution file and build/run scripts)
 - Run the "build.bat" file. 
This will rebuild entire solution in release mode.




Running Solution:

 - Run the "Run.bat" file 

 - This will open following 6 windows
       
   - Repository
       
   - Test Harness
       
   - Build Server
   - Builder 1
   - Builder 2
   - Client GUI
   - Client GUI Console (To display processing of GUI Window)
       
--------------------------------------------------------------------------


Demonstration:

 - 
For demo, the follwing test projects are created
     1. Sample Code
     2. Exception Code
     3. Dependency Analyzer
 - For each project, the following operations are executed
    - Check-in files
    - Build Library
    - Send to Test Harness
    - Get Test Results
    - Checkout files