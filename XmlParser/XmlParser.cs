﻿/**
 * XML Parser
 * Parses Test Request implemented as XML file to extract test driver & library
 * 
 * FileName     : XmlParser.cs
 * Author       : Sahil Gupta
 * Source       : Jim Fawcett
 * Date         : 24 September 2016 
 * Version      : 1.0
 * 
 * Public Interface
 * ----------------
 * XmlParser()
 *  - Constructor to initialize new instance of test request
 *  bool ParseXmlFile(string XmlFilePath)
 *   - Read XML from file and fill in-memory data-structure xmlTestInfoList to access it
 *  bool ParseXmlString(string Xml)
 *   - Read XML from string and fill in-memory data-structure xmlTestInfoList to access it
 *  void DisplayTestRequest()
 *   - display test request fields read from XML
 * 
 * Build Process
 * -------------
 * - Required files:   NA
 * 
 * Maintenance History
 * -------------------
 * ver 1.0 : 24 September 2016
 *  - first release
 */

using System;
using System.IO;
using System.Linq;
using System.Xml.Linq;
using System.Collections.Generic;

namespace XMLParser
{
    public class xmlTestInfo
    {
        /**********************************************************************
                             M E M B E R S
        **********************************************************************/

        public int _Version { get; set; }
        public string _Author { get; set; }
        public DateTime _TimeStamp { get; set; }

        public string _TestName { get; set; }
        public string _refCommitId { get; set; }
        public string _BuildVersion { get; set; }
        public string _TestDriver { get; set; }
        public List<string> _TestCode { get; set; }
        public List<string> _TestSrc { get; set; }

        /**********************************************************************
                             P U B L I C   M E T H O D S
        **********************************************************************/

        /**
         * Display
         * Display on console contents of current xmlTestInfo instance
         */
        public void Display()
        {
            Console.WriteLine("  {0,-12} : {1}", "Version", _Version);
            Console.WriteLine("  {0,-12} : {1}", "Author", _Author);
            Console.WriteLine("  {0,-12} : {1}", "TimeStamp", _TimeStamp);
            Console.WriteLine("  {0,-12} : {1}", "TestName", _TestName);
            Console.WriteLine("  {0,-12} : {1}", "CommitId", _refCommitId);
            Console.WriteLine("  {0,-12} : {1}", "BuildVersion", _BuildVersion);
            Console.WriteLine("  {0,-12} : {1}", "TestDriver", _TestDriver);

            foreach (string Library  in _TestCode)
            {
                Console.WriteLine("  {0,-12} : {1}", "Library", Library);
            }
            foreach (string Source in _TestSrc)
            {
                Console.WriteLine("  {0,-12} : {1}", "Source", Source);
            }
            Console.WriteLine("");
        }
    }

    public class xmlBuildInfo
    {
        /**********************************************************************
                             M E M B E R S
        **********************************************************************/

        public int _Version { get; set; }
        public string _Author { get; set; }
        public DateTime _TimeStamp { get; set; }
        public string _refCommitId { get; set; }

        public string _Name { get; set; }
        public string _bVer { get; set; }
        public string _Src { get; set; }
        public List<string> _Ref { get; set; }
        public List<string> _Srcs { get; set; }

        /**********************************************************************
                             P U B L I C   M E T H O D S
        **********************************************************************/

        /**
         * Display
         * Display on console contents of current xmlBuildInfo instance
         */
        public void Display()
        {
            Console.WriteLine("  {0,-12} : {1}", "Version", _Version);
            Console.WriteLine("  {0,-12} : {1}", "Author", _Author);
            Console.WriteLine("  {0,-12} : {1}", "TimeStamp", _TimeStamp);
            Console.WriteLine("  {0,-12} : {1}", "CommitId", _refCommitId);
            Console.WriteLine("  {0,-12} : {1}", "Name", _Name);
            Console.WriteLine("  {0,-12} : {1}", "BuildVersion", _bVer);
            Console.WriteLine("  {0,-12} : {1}", "Source", _Src);

            foreach (string Ref in _Ref)
            {
                Console.WriteLine("  {0,-12} : {1}", "Reference", Ref);
            }
            foreach (string Src in _Srcs)
            {
                Console.WriteLine("  {0,-12} : {1}", "Sources", Src);
            }
            Console.WriteLine("");
        }
    }

    public class xmlRepoInfo
    {
        /**********************************************************************
                             M E M B E R S
        **********************************************************************/

        public int _Version { get; set; }
        public string _Author { get; set; }
        public DateTime _TimeStamp { get; set; }

        public string _CommitId { get; set; }
        public string _bVersion { get; set; }
        public string _Title { get; set; }
        public string _Description { get; set; }
        public List<string> _Src { get; set; }


        /**********************************************************************
                             P U B L I C   M E T H O D S
        **********************************************************************/

        /**
         * Display
         * Display on console contents of current xmlBuildInfo instance
         */
        public void Display()
        {
            Console.WriteLine("  {0,-12} : {1}", "Version", _Version);
            Console.WriteLine("  {0,-12} : {1}", "Author", _Author);
            Console.WriteLine("  {0,-12} : {1}", "TimeStamp", _TimeStamp);
            Console.WriteLine("  {0,-12} : {1}", "CommitId", _CommitId);
            Console.WriteLine("  {0,-12} : {1}", "BuildVersion", _bVersion);
            Console.WriteLine("  {0,-12} : {1}", "Title", _Title);
            Console.WriteLine("  {0,-12} : {1}", "Description", _Description);

            foreach (string Source in _Src)
            {
                Console.WriteLine("  {0,-12} : {1}", "Source", Source);
            }
            Console.WriteLine("");
        }
    }

    public abstract class XmlParser
    {
        protected XDocument _xDoc;

        /**
         * ParseXmlFile
         * Read XML file and fill in-memory data-structure xmlTestInfoList to access it
         */
        public bool ParseXmlFile(string XmlFilePath)
        {
            bool bRet;
            FileStream XML = null;
            try
            {
                //Console.WriteLine("\n>>>>Parsing Test Request File (AD:{0})<<<<", AppDomain.CurrentDomain.FriendlyName);

                /* Open XML file */
                XML = new FileStream(XmlFilePath, System.IO.FileMode.Open, FileAccess.Read, FileShare.Read);

                /* Load contents of XML file */
                _xDoc = XDocument.Load(XML);

                bRet = ParseXml();
            }
            catch (Exception Ex)
            {
                Console.WriteLine("Exception : {0} \nStack Trace:\n{1}", Ex.Message, Ex.StackTrace);
                return false;
            }
            finally
            {
                if (null != XML)
                    XML.Close();
            }

            return bRet;
        }

        public bool ParseXmlString(string Xml)
        {
            _xDoc = XDocument.Parse(Xml);
            return ParseXml();
        }

        protected DateTime parseTime(string dtStr)
        {
            if (dtStr == null)
            {
                return DateTime.Now;
            }

            DateTime dt = DateTime.ParseExact(dtStr, @"MM/dd/yyyy HH:mm:ss:fffffff", null);
            if (dt == null)
            {
                dt = DateTime.Parse(dtStr);
            }

            return dt;
        }

        protected abstract bool ParseXml();
        public abstract void DisplayXml();
    };

    public class XmlTesterParser : XmlParser
    {
        /**********************************************************************
                         M E M B E R S
         **********************************************************************/

        public List<xmlTestInfo> _xmlTestInfoList { get; set; }

        /**********************************************************************
                     P U B L I C   M E T H O D S
        **********************************************************************/

        public XmlTesterParser()
        {
            _xDoc = new XDocument();
            _xmlTestInfoList = new List<xmlTestInfo>();
        }

        protected override bool ParseXml()
        {
            try
            {
                /* Get XML tags */
                string Version = _xDoc.Descendants("Version").First().Value;
                string Author = _xDoc.Descendants("Author").First().Value;
                XElement[] xElement = _xDoc.Descendants("Test").ToArray();

                /* Loop for each test driver, extract and store information in xmlTestList */
                int TestCnt = xElement.Count();
                for (int i = 0; i < TestCnt; i++)
                {
                    xmlTestInfo TestInfo = GetNewTestInfo(Author, Version, xElement[i]);
                    _xmlTestInfoList.Add(TestInfo);
                }
            }
            catch (Exception Ex)
            {
                Console.WriteLine("Exception : {0} \nStack Trace:\n{1}", Ex.Message, Ex.StackTrace);
                return false;
            }

            return true;
        }

        /**
         * GetNewTestInfo
         * Creates new TestInfo object and fills it with data from XML element
         */
        private xmlTestInfo GetNewTestInfo(string Author, string Version, XElement xElement)
        {
            xmlTestInfo TestInfo = null;

            try
            {
                TestInfo = new xmlTestInfo();

                TestInfo._Version = Int32.Parse(Version);
                TestInfo._Author = Author;
                if (null != xElement.Element("TimeStamp"))
                    TestInfo._TimeStamp = DateTime.Parse(xElement.Element("TimeStamp").Value);
                else
                    TestInfo._TimeStamp = DateTime.Now;
                TestInfo._TestName = xElement.Attribute("Name").Value;
                TestInfo._refCommitId = xElement.Element("CommitId").Value;
                TestInfo._BuildVersion = xElement.Element("BuildVersion").Value;
                TestInfo._TestDriver = xElement.Element("TestDriver").Value;

                TestInfo._TestCode = new List<string>();
                IEnumerable<XElement> xTestCode = xElement.Elements("Library");
                foreach (var library in xTestCode)
                {
                    TestInfo._TestCode.Add(library.Value);
                }

                TestInfo._TestSrc = new List<string>();
                IEnumerable<XElement> xTestSrc = xElement.Elements("Source");
                foreach (var source in xTestSrc)
                {
                    TestInfo._TestSrc.Add(source.Value);
                }

            }
            catch (Exception Ex)
            {
                Console.WriteLine("Exception : {0}", Ex.Message);
            }

            return TestInfo;
        }

        /**
         * DisplayTestRequest
         * Prints on console, information from _xmlTestLinfoList of current instance
         */
        public override void DisplayXml()
        {
            Console.WriteLine("\n-------------------- XML FILE---------------------");
            foreach (xmlTestInfo TestInfo in _xmlTestInfoList)
            {
                TestInfo.Display();
            }
            Console.WriteLine("--------------------------------------------------");
        }
    };

    public class XmlBuilderParser : XmlParser
    {
        /**********************************************************************
                         M E M B E R S
         **********************************************************************/
        
        public List<xmlBuildInfo> _xmlInfoList { get; set; }
        
        /**********************************************************************
                     P U B L I C   M E T H O D S
        **********************************************************************/

        public XmlBuilderParser()
        {
            _xDoc = new XDocument();
            _xmlInfoList = new List<xmlBuildInfo>();
        }

        protected override bool ParseXml()
        {
            try
            {
                /* Get XML tags */
                string Commit = null;

                string Version = _xDoc.Descendants("Version").First().Value;
                string Author = _xDoc.Descendants("Author").First().Value;
                if(_xDoc.Descendants("CommitId").First().Value != "")
                    Commit = _xDoc.Descendants("CommitId").First().Value;
                XElement[] xElement = _xDoc.Descendants("Build").ToArray();

                /* Loop for each test driver, extract and store information in xmlTestList */
                int TestCnt = xElement.Count();
                for (int i = 0; i < TestCnt; i++)
                {
                    xmlBuildInfo Info = GetNewBuildInfo(Author, Version, Commit, xElement[i]);
                    _xmlInfoList.Add(Info);
                }
            }
            catch (Exception Ex)
            {
                Console.WriteLine("Exception : {0} \nStack Trace:\n{1}", Ex.Message, Ex.StackTrace);
                return false;
            }

            return true;
        }

        /**
         * GetNewTestInfo
         * Creates new TestInfo object and fills it with data from XML element
         */
        private xmlBuildInfo GetNewBuildInfo(string Author, string Version, string commit, XElement xElement)
        {
            xmlBuildInfo Info = null;

            try
            {
                Info = new xmlBuildInfo();

                Info._Version = Int32.Parse(Version);
                Info._Author = Author;
                Info._refCommitId = commit;
                if(null != xElement.Element("TimeStamp"))
                    Info._TimeStamp = DateTime.Parse(xElement.Element("TimeStamp").Value);
                else
                    Info._TimeStamp = DateTime.Now;
                Info._Name = xElement.Attribute("Name").Value;
                Info._bVer = xElement.Element("BuildVersion").Value;
                if (null != xElement.Element("Source"))
                    Info._Src = xElement.Element("Source").Value;
                Info._Ref = new List<string>();
                IEnumerable<XElement> xTestRef = xElement.Elements("Ref");
                foreach (var Ref in xTestRef)
                {
                    Info._Ref.Add(Ref.Value);
                }
                Info._Srcs = new List<string>();
                IEnumerable<XElement> xSrcs = xElement.Elements("Src");
                foreach (var Src in xSrcs)
                {
                    Info._Srcs.Add(Src.Value);
                }

            }
            catch (Exception Ex)
            {
                Console.WriteLine("Exception : {0}", Ex.Message);
            }

            return Info;
        }

        /**
         * DisplayTestRequest
         * Prints on console, information from _xmlTestLinfoList of current instance
         */
        public override void DisplayXml()
        {
            Console.WriteLine("\n-------------------- XML FILE---------------------");
            foreach (xmlBuildInfo Info in _xmlInfoList)
            {
                Info.Display();
            }
            Console.WriteLine("--------------------------------------------------");
        }
    };

    public class XmlRepoParser : XmlParser
    {
        /**********************************************************************
                         M E M B E R S
         **********************************************************************/

        public List<xmlRepoInfo> _xmlInfoList { get; set; }

        /**********************************************************************
                     P U B L I C   M E T H O D S
        **********************************************************************/

        public XmlRepoParser()
        {
            _xDoc = new XDocument();
            _xmlInfoList = new List<xmlRepoInfo>();
        }

        protected override bool ParseXml()
        {
            try
            {
                /* Get XML tags */
                string Version = _xDoc.Descendants("Version").First().Value;
                string Author = _xDoc.Descendants("Author").First().Value;
                DateTime dt = parseTime(_xDoc.Descendants("TimeStamp").First().Value);
                XElement[] xElement = _xDoc.Descendants("Commit").ToArray();

                /* Loop for each test driver, extract and store information in xmlTestList */
                int TestCnt = xElement.Count();
                for (int i = 0; i < TestCnt; i++)
                {
                    xmlRepoInfo Info = GetNewRepoInfo(Author, Version, dt, xElement[i]);
                    _xmlInfoList.Add(Info);
                }
            }
            catch (Exception Ex)
            {
                Console.WriteLine("Exception : {0} \nStack Trace:\n{1}", Ex.Message, Ex.StackTrace);
                return false;
            }

            return true;
        }

        /**
         * GetNewTestInfo
         * Creates new TestInfo object and fills it with data from XML element
         */
        private xmlRepoInfo GetNewRepoInfo(string Author, string Version, DateTime dt, XElement xElement)
        {
            xmlRepoInfo Info = null;

            try
            {
                Info = new xmlRepoInfo();

                Info._Version = Int32.Parse(Version);
                Info._Author = Author;
                Info._TimeStamp = dt;
                if (null != xElement.Attribute("Id"))
                    Info._CommitId = xElement.Attribute("Id").Value;
                if (null != xElement.Element("Build"))
                    Info._bVersion = xElement.Element("Build").Attribute("Version").Value;
                Info._Title = xElement.Element("Title").Value;
                Info._Description = xElement.Element("Description").Value;

                Info._Src = new List<string>();
                IEnumerable<XElement> xTestSrc = xElement.Elements("Source");
                foreach (var source in xTestSrc)
                {
                    Info._Src.Add(source.Value);
                }

            }
            catch (Exception Ex)
            {
                Console.WriteLine("Exception : {0}", Ex.Message);
            }

            return Info;
        }

        /**
         * Display
         * Prints on console, information from _xmlInfoList of current instance
         */
        public override void DisplayXml()
        {
            Console.WriteLine("\n-------------------- XML FILE---------------------");
            foreach (xmlRepoInfo Info in _xmlInfoList)
            {
                Info.Display();
            }
            Console.WriteLine("--------------------------------------------------");
        }
    };

    class TestXml
    {

        static void Main(string[] args)
        {
            bool bRet;
            string XmlPath = @"..\..\..\TestRequest\SampleCodeTestRequest.xml";

            XmlTesterParser Parser = new XmlTesterParser();

            bRet = Parser.ParseXmlFile(XmlPath);
            if (false == bRet)
            {
                Console.WriteLine("Error: Parser.ParseTestRequest({0})...FAILED", XmlPath);
                return;
            }

            Parser.DisplayXml();
        }

    };
}