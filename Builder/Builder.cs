﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace BuildServer
{
    using TestInterface;
    using Communication;
    using nsBlockingQueue;
    using FileHosting;
    using FileManager;
    using System.IO;
    using System.Diagnostics;
    using DependencyAnalyzer;

    class Builder
    {
        string URL;
        string LocalRepo;

        string RepoServerUrl;
        string FileHostingUrl;
        string BuildServerUrl;

        Thread tSender;
        Sender MySender;
        BlockingQueue<Message> SendQ;

        Thread tReceiver;
        Receiver MyReceiver;

        FileStreamer Streamer;

        public Builder(string MyUrl)
        {
            Console.WriteLine("Starting Builder at URL ({0})", MyUrl);
            this.URL = MyUrl;

            SendQ = new BlockingQueue<Message>();
            MySender = new Sender();
            tSender = new Thread(ThreadSender);
            tSender.Start();

            //ReceiveQ = new BlockingQueue<Message>();
            MyReceiver = new Receiver();
            tReceiver = new Thread(ThreadReceiver);
            tReceiver.Start();
        }

        ~Builder()
        {
            /* Stop Reciever first because if we stop sender and we recieve a msg,
             * then we cannot send the results of that msg */
            tReceiver.Join();

            tSender.Join();
        }

        public void configureBuilder(string LocalRepo, string RepoUrl, string FileHostingUrl)
        {
            this.LocalRepo = LocalRepo;
            this.RepoServerUrl = RepoUrl;
            this.FileHostingUrl = FileHostingUrl;

            Streamer = new FileStreamer(FileHostingUrl);
        }

        public void configureBuildServer(string buildServerUrl)
        {
            BuildServerUrl = buildServerUrl;
            BuilderReady();
        }

        void ThreadSender()
        {
            while (true)
            {
                Message msg = SendQ.deQ();

                if (msg.Type == Message.MessageType.Quit) {
                    Console.WriteLine("Quitting...");
                    break;
                }

                Console.WriteLine("\nSending WCF Message ({0}) to ({1}).", msg.Type.ToString(), msg.ToUrl);
                MySender.CreateNewSession(msg.ToUrl);
                MySender.SendMessage(msg);
            }
        }        

        void BuilderReady()
        {
            MessageBuilder readyMsg = new MessageBuilder(Message.MessageType.Ready);
            readyMsg.BuildMessage(URL, BuildServerUrl, null, null);

            SendQ.enQ(readyMsg.GetMessage());
        }

        void AckBuildRequest(ref BuildMessage bMsg, string SourceUrl)
        {
            MessageBuilder BuildRequestAckMsg = new MessageBuilder(Message.MessageType.BuildAck);
            BuildRequestAckMsg.BuildMessage(URL, SourceUrl, bMsg.Author, bMsg.GetBuildResultXML());

            SendQ.enQ(BuildRequestAckMsg.GetMessage());
        }

        void StoreBuildResult(ref BuildMessage bMsg)
        {
            MessageBuilder LogStore = new MessageBuilder(Message.MessageType.StoreBuildLogMsg);
            LogStore.BuildMessage(URL, RepoServerUrl, bMsg.Author, bMsg.GetBuildResultXML());

            SendQ.enQ(LogStore.GetMessage());
        }

        void ThreadReceiver()
        {
            Message Msg;
            BuildMessage bMsg;

            MyReceiver.Init(URL);

            Console.WriteLine("Waiting for Messages...");
            while (true)
            {
                //TODO : Delegate Work to another thread
                Msg = MyReceiver.GetMessage();

                Console.WriteLine("PROCESSING START ({0}) from ({1}).", Msg.Type.ToString(), Msg.FromUrl);

                switch (Msg.Type)
                {
                    case Message.MessageType.BuildRequest:
                        bMsg = new BuildMessage();
                        bMsg.ParseBuildRequestXML(Msg.Payload); // parse result from XML in structure
                        processBuildRequest(ref bMsg, Msg.FromUrl);
                        BuilderReady();
                        break;

                    case Message.MessageType.Quit:
                        SendQ.enQ(Msg);
                        return;

                    default:
                        Console.WriteLine("Unknown Message ({0}) Received:", Msg.Type.ToString());
                        break;
                }
                Console.WriteLine("PROCESSING END ({0})", Msg.Type.ToString());
            }
        }

        private bool validateBuildRequest(ref BuildMessage bMsg)
        {
            if (bMsg.refCommitId == null)
            {
                bMsg.BuildStatus = false;
                bMsg.BuildResult = "Reference Commit ID Missing";
                return false;
            }

            if (bMsg.SourceFile == null && bMsg.Sources.Count == 0)
            {
                bMsg.BuildStatus = false;
                bMsg.BuildResult = "Invalid Build Request. Must have source file";
                return false;
            }

            return true;
        }

        private void processBuildRequest(ref BuildMessage bMsg, string SourceUrl)
        {
            try
            {
                if (false == validateBuildRequest(ref bMsg))
                {
                    return;
                }

                if (false == downloadBuildSrcFiles(ref bMsg))
                {
                    bMsg.BuildStatus = false;
                    bMsg.BuildResult += "File Not Found in commit";
                    return;
                }

                if (false == buildLibrary(ref bMsg))
                {
                    return;
                }

                // Upload build to repository
                uploadBuildLibFiles(ref bMsg);
            }
            finally
            {
                //Send Acknowledgement message to client
                AckBuildRequest(ref bMsg, SourceUrl);

                //Save Log on Repository
                StoreBuildResult(ref bMsg);

                //DeleteFiles(ref bMsg);
            }
        }

        private bool downloadFile(string location, string filename, string commitId)
        {
            //Check if file is available in Repository
            if (false == Streamer.CheckFileAvailability(filename, commitId))
            {
                Console.WriteLine("Error: File {0} not found on Repository.", filename);
                return false;
            }

            //Download file from repository
            Console.WriteLine("Downloading file {0} using Streaming.", filename);
            Streamer.DownloadFile(location, filename, commitId);
            return true;
        }

        private bool downloadBuildSrcFiles(ref BuildMessage bMsg)
        {
            bool status = false;
            string localDir = getLocalDir(ref bMsg);
            try
            {
                if (bMsg.SourceFile != null)
                {
                    status = downloadFile(localDir, bMsg.SourceFile, bMsg.refCommitId);
                    if (status == false)
                    {
                        return false;
                    }
                }

                foreach (var Ref in bMsg.References)
                {
                    status = downloadFile(localDir, Ref, bMsg.refCommitId);
                    if (status == false)
                    {
                        return false;
                    }
                }

                foreach (var Src in bMsg.Sources)
                {
                    status = downloadFile(localDir, Src, bMsg.refCommitId);
                    if (status == false)
                    {
                        return false;
                    }
                }

                status = true;
            }
            catch (Exception Ex)
            {
                Console.WriteLine("Exception : ({0})", Ex.Message);
            }
            return status;
        }

        private void DeleteFiles(ref BuildMessage bMsg)
        {
            string localDir = getLocalDir(ref bMsg);

            if (bMsg.SourceFile != null && File.Exists(Path.Combine(localDir, bMsg.SourceFile)))
            {
                Console.WriteLine("Deleting Code file {0}", bMsg.SourceFile);
                //File.SetAttributes(file, FileAttributes.Normal);
                File.Delete(Path.Combine(localDir, bMsg.SourceFile));
            }

            //Delete test code files if they exists    
            foreach (var Ref in bMsg.References)
            {
                if (File.Exists(Path.Combine(localDir, Ref)))
                {
                    Console.WriteLine("Deleting Code file {0}", Ref);
                    //File.SetAttributes(file, FileAttributes.Normal);
                    File.Delete(Path.Combine(localDir, Ref));
                }
            }

            //Delete src code files if they exists    
            foreach (var Src in bMsg.Sources)
            {
                if (File.Exists(Path.Combine(localDir, Src)))
                {
                    Console.WriteLine("Deleting Code file {0}", Src);
                    //File.SetAttributes(file, FileAttributes.Normal);
                    File.Delete(Path.Combine(localDir, Src));
                }
            }

            //File.SetAttributes(targetDir, FileAttributes.Normal);
            Directory.Delete(localDir, false);
        }

        private void uploadBuildLibFiles(ref BuildMessage bMsg)
        {
            //Send Test Code Files to Repository
            foreach (string file in bMsg.Sources)
            {
                string libname = getAssembleNameFormFileName(file);
                string libpath = Path.Combine(getLocalDir(ref bMsg), libname);

                Console.WriteLine("Uploading source file ({0}) to Repository using Streaming.", libname);
                Streamer.UploadFile(libpath, bMsg.refCommitId);
            }
        }


        private string getLocalDir(ref BuildMessage bMsg)
        {
            string LocalDir = LocalRepo + "\\" + bMsg.refCommitId;

            if (!Directory.Exists(LocalDir))
                Directory.CreateDirectory(LocalDir);

            return LocalDir;
        }

        private string getBuildCommand(string location, string sourcefile, List<string> references, out string libname)
        {
            int filecnt = 0;
            libname = getAssembleNameFormFileName(sourcefile);
            string libCommand = @"/t:library /out:" + libname;
            StringBuilder cmd = new StringBuilder(libCommand);

            foreach (string lib in Directory.EnumerateFiles(location, "*.dll", SearchOption.AllDirectories))
            {
                string libName = Path.GetFileName(lib);

                // Build only files contained in Build request
                if (references.Contains(libName))
                {
                    Console.WriteLine("The reference files are {0}", libName);
                    cmd.Append(" /r:");
                    cmd.Append(libName);
                    ++filecnt;
                }
            }

            string mainfile = null;
            foreach (string file in Directory.EnumerateFiles(location, "*.cs", SearchOption.AllDirectories))
            {
                string fileName = Path.GetFileName(file);

                if (sourcefile.Equals(fileName))
                {
                    mainfile = fileName;
                    Console.WriteLine("The main file is {0}", fileName);
                }

                // Build only files contained in Build request
                if (references.Contains(fileName))
                {
                    Console.WriteLine("The reference files are {0}", fileName);
                    cmd.Append(" /r:");
                    cmd.Append(fileName);
                    ++filecnt;
                }
            }

            if (mainfile == null || references.Count != filecnt)
            {
                Console.WriteLine("Error: Reference count mismatch | Main file not found");
                throw new Exception("Error: Reference count mismatch | Main file not found");
            }

            cmd.Append(" ");
            cmd.Append(mainfile);

            return cmd.ToString();
        }

        public Dictionary<string, List<string>> getBuildOrder(string location, ref BuildMessage bMsg)
        {
            DepAnal da = new DepAnal();

            foreach (string file in bMsg.References)
            {
                da.BuildDepTable(Path.Combine(location, file));
            }

            foreach (string file in bMsg.Sources)
            {
                da.BuildDepTable(Path.Combine(location, file));
            }

            da.showDependencies();
            da.showDefinitions();

            BuildOrder bo = new BuildOrder();
            Dictionary<string, List<string>> dep = new Dictionary<string, List<string>>();

            foreach (string filename in bMsg.Sources)
            {
                List<string> references = da.DependsOn(Path.Combine(location, filename));
                dep.Add(filename, references);

                Console.Write("{0} depends on : ", filename);
                foreach (string file in references)
                {
                    Console.Write("{0} ", file);
                }
                Console.WriteLine("");
            }

            List<string> order = bo.getBuildOrder(dep);

            bMsg.BuildResult += "Build Order : ";
            Console.Write("\nBuild Order : ");
            foreach (string name in order) {
                bMsg.BuildResult += name + " ";
                Console.Write("{0} ", name);
            }
            Console.WriteLine("");
            bMsg.BuildResult += "\n";

            Dictionary<string, List<string>> BuildList = new Dictionary<string, List<string>>();
            foreach (string name in order)
            {
                List<string> refs;
                dep.TryGetValue(name, out refs);
                BuildList.Add(name, refs);
            }

            return BuildList;
        }

        private string getAssembleNameFormFileName(string filename)
        {
            if (filename.Equals("ITest.cs")) return "TestInterface.dll";
            else return Path.GetFileNameWithoutExtension(filename) + ".dll";
        }

        private List<string> getReferenceAssemblyForFiles(List<string> files)
        {
            List<string> refs = new List<string>();

            if (files == null)
            {
                return refs;
            }

            foreach (string file in files)
            {
                refs.Add(getAssembleNameFormFileName(file));
            }

            return refs;
        }

        public bool buildLibrary(ref BuildMessage bMsg)
        {
            string localDir = getLocalDir(ref bMsg);
            //Console.WriteLine("localDir : " + localDir);

            var frameworkPath = System.Runtime.InteropServices.RuntimeEnvironment.GetRuntimeDirectory();
            var cscPath = Path.Combine(frameworkPath, "csc.exe");
            Console.WriteLine("cscPath : " + cscPath);

            Dictionary<string, List<string>> bo = getBuildOrder(localDir, ref bMsg);

            foreach (var resource in bo)
            {
                string result;
                string libName;
                Console.WriteLine("Building Library : {0}", resource.Key);

                string buildCommand = getBuildCommand(localDir, resource.Key, getReferenceAssemblyForFiles(resource.Value), out libName);
                bMsg.BuildStatus = generateBuild(cscPath, localDir, buildCommand, out result);
                if (bMsg.BuildStatus == false)
                {
                    bMsg.BuildResult += "Failed to build Library : " + resource.Key + ".\n";
                    break;
                }

                if (bMsg.Sources.Contains(resource.Key))
                {
                    bMsg.Libraries.Add(libName);
                    bMsg.BuildResult += "Succeeded building Library : " + resource.Key + ".\n";
                    bMsg.BuildResult += result;
                    bMsg.BuildResult += "\n";
                }
            }

            return bMsg.BuildStatus;
        }

        private bool generateBuild(string executable, string workingdir, string buildCommand, out string status)
        {
            bool ret;
            try
            {
                Process p = new Process();
                p.StartInfo.FileName = executable;
                p.StartInfo.WorkingDirectory = workingdir;
                p.StartInfo.Arguments = buildCommand;
                Console.WriteLine("Build Command : " + p.StartInfo.Arguments);
                p.StartInfo.UseShellExecute = false;
                p.StartInfo.RedirectStandardOutput = true;
                p.Start();
                p.WaitForExit();

                //Console.WriteLine("HasExited : " + p.HasExited);
                //Console.WriteLine("Exit Code : " + p.ExitCode);

                ret = (p.ExitCode == 0) ? true : false;
                status = p.StandardOutput.ReadToEnd();
                Console.WriteLine("Build Result : " + status);
            }
            catch (Exception e)
            {
                ret = false;
                status = "\nException : " + e.GetType();
                Console.WriteLine("Build Result : " + status);

            }
            return ret;
        }


        static void Main(string[] args)
        {
            string URL = @"http://localhost:4000/ICommunication/BuildServer";
            
            string RepoUrl = @"http://localhost:4000/ICommunication/Repository";
            string FileHostingUrl = @"http://localhost:4000/IStream/FileHosting";
            string LocalRepositoryPath = "Repository\\BuildSrvDir";

            if (args.Count() > 0)  //Invoked by Build Server passing URL
            {
                URL = args[0];
            }

            Console.Title = "Builder (" + URL + ")";

            Builder builder = new Builder(URL);
            builder.configureBuilder(LocalRepositoryPath, RepoUrl, FileHostingUrl);

            if (args.Count() > 1)  //Invoked by Build Server its URL for discovery
            {
                builder.configureBuildServer(args[1]);
            }

#if _TEST_BUILD_ORDER
            BuildOrder bo = new BuildOrder();
            Dictionary<string, List<string>> dep = new Dictionary<string, List<string>>();
            dep.Add("a", new List<string> { "f" });
            dep.Add("b", new List<string> { "f" });
            dep.Add("c", new List<string> { "d" });
            dep.Add("d", new List<string> { "a", "b" });
            dep.Add("e", new List<string>());
            dep.Add("f", new List<string>());

            List<string> order = bo.getBuildOrder(dep);

            Console.Write("Build Order : ");
            foreach (string name in order)
                Console.Write("{0} ", name);
            Console.WriteLine("");
#endif
        }
    }
}