﻿/**
 * Client
 * A remote process instance that sends files to test harness for testing
 *      
 * FileName     : Client.cs
 * Author       : Sahil Gupta
 * Date         : 16 November 2016 
 * Version      : 1.0
 * 
 * Public Interface
 * ----------------
 * Client(string MyUrl)
 *  - create new instance of client sending requests from specified URL
 * void ConfigureURL(string TestHarnessUrl, string RepositoryUrl, string FileHostingUrl)
 *  - set URL path for repo and test harness
 * void SendTestRequest(TestMessage tMsg)
 *  - send new test request to test harness
 * void GetTestResult(TestMessage tMsg)
 *  - send new query for test result to test harness
 * Message ProcessTestResult()
 *  - get message containing test result
 *  
 * Build Process
 * -------------
 * - Required files: ITest.cs, BlockingQueue.cs, MessageBuilder.cs, MsgParser.cs, Sender.cs, Receiver.cs FileStreamer.cs
 * 
 * Maintenance History
 * -------------------
 * ver 1.0 : 16 November 2016
 *  - first release
 */

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace nsClient
{
    using TestInterface;
    using Communication;
    using nsBlockingQueue;
    using FileHosting;

    public class Client
    {
        string URL;
        string TestHarnessURL;
        string RepositoryURL;
        string BuildServerURL;

        const string checkoutDefault = ".";

        Thread tSender;
        Sender MySender;
        BlockingQueue<Message> SendQ;

        FileStreamer Streamer;

        Thread tReceiver;
        Receiver MyReceiver;
        BlockingQueue<Message> ReceiveQ;

        BlockingQueue<Message> ResultQ;

        Dictionary<string, string> MessageStore;

        public Client(string MyUrl)
        {
            Console.WriteLine("Starting Client at URL ({0})", MyUrl);
            URL = MyUrl;

            MessageStore = new Dictionary<string, string>();

            ResultQ = new BlockingQueue<Message>();

            // Create and start receiver to recieve messages
            ReceiveQ = new BlockingQueue<Message>();
            MyReceiver = new Receiver();
            tReceiver = new Thread(ThreadReceiver);
            tReceiver.Start();

            // create and start sender to send message
            SendQ = new BlockingQueue<Message>();
            MySender = new Sender();
            tSender = new Thread(ThreadSender);
            tSender.Start();
        }

        ~Client()
        {
            /* Stop Reciever first because if we stop sender and we recieve a msg,
             * then we cannot send the results of that msg */
            tReceiver.Join();

            tSender.Join();
        }

        public void ConfigureURL(string TestHarnessUrl, string RepositoryUrl, string BuildServerUrl, string FileHostingUrl)
        {
            TestHarnessURL = TestHarnessUrl;
            RepositoryURL = RepositoryUrl;
            BuildServerURL = BuildServerUrl;
            Streamer = new FileStreamer(FileHostingUrl);
        }

        private void ThreadSender()
        {
            while (true)
            {
                //Wait for new message from user
                Message msg = SendQ.deQ();

                //Connect to process to which we need to send message
                MySender.CreateNewSession(msg.ToUrl);

                //Send message using WCF
                Console.WriteLine("Sending WCF Message ({0}) to {1}", msg.Type.ToString(), msg.ToUrl);
                MySender.SendMessage(msg);

                if (msg.Type == Message.MessageType.Quit && msg.ToUrl == URL) {
                    Console.WriteLine("Thread Sender Quitting...");
                    return;
                }
            }
        }

        private void ThreadReceiver()
        {
            Message Msg;
            TestMessage tMsg;
            BuildMessage bMsg;
            RepoMessage rMsg;

            MyReceiver.Init(URL);
            Console.WriteLine("Waiting for Messages...");

            while (true) {
                //Wait for acknowledgement messages for sent requests
                Msg = MyReceiver.GetMessage();

                Console.WriteLine("PROCESSING START ({0}) from ({1})", Msg.Type.ToString(), Msg.FromUrl);

                switch (Msg.Type) {
                    case Message.MessageType.RepoInfo:
                        ResultQ.enQ(Msg);   //Enqueue for GUI window to process
                        break;

                    case Message.MessageType.GetCommitInfo:
                        ResultQ.enQ(Msg);   //Enqueue for GUI window to process
                        break;

                    case Message.MessageType.CommitInitiated:
                        rMsg = new RepoMessage();
                        rMsg.ParseRepoResultXML(Msg.Payload);
                        processCommitInit(ref rMsg);
                        break;

                    case Message.MessageType.CommitCompleted:
                        rMsg = new RepoMessage();
                        rMsg.ParseRepoResultXML(Msg.Payload);
                        processCommitCompleted(ref rMsg);
                        ResultQ.enQ(Msg);   //Enqueue for GUI window to process
                        break;

                    case Message.MessageType.BuildAck:
                        bMsg = new BuildMessage();
                        bMsg.ParseBuildResultXML(Msg.Payload);
                        processBuildAck(ref bMsg);
                        ResultQ.enQ(Msg);   //Enqueue for GUI window to process
                        break;

                    case Message.MessageType.TestRequestAck:
                    case Message.MessageType.TestLogsAck:
                        //Parse result form message
                        tMsg = new TestMessage();
                        tMsg.ParseTestResultXML(Msg.Payload);
                        tMsg.duration = DateTime.Now.Subtract(Msg.TimeStamp);
                        Console.WriteLine("Overall Latency for sending message and receiving reply : {0}", tMsg.duration);
                        tMsg.DisplayResult();
                        ResultQ.enQ(Msg);   //Enqueue for GUI window to process
                        break;

                    case Message.MessageType.Checkout:
                        rMsg = new RepoMessage();
                        rMsg.ParseRepoResultXML(Msg.Payload);
                        processCheckoutResp(ref rMsg);
                        ResultQ.enQ(Msg);   //Enqueue for GUI window to process
                        break;

                    case Message.MessageType.Quit:
                        ResultQ.enQ(Msg);   //Enqueue for GUI window to process
                        Console.WriteLine("Thread Reciever Quitting...");
                        return;

                    default:
                        Console.WriteLine("Unknown Message ({0}) Received:", Msg.Type.ToString());
                        break;
                }
                Console.WriteLine("PROCESSING END ({0})", Msg.Type.ToString());
            }
        }

        /**
         *  Function call for GUI to get client result.
         */
        public Message ProcessClientResult()
        {
            return ResultQ.deQ();
        }

        //
        // Build Server Interactions
        //

        public void SendBuildRequest(BuildMessage bMsg)
        {
            MessageBuilder BuildRequestMsg = new MessageBuilder(Message.MessageType.BuildRequest);
            //Console.WriteLine("Build Request XML : \n" + bMsg.GetBuildRequestXML() + "\n");
            BuildRequestMsg.BuildMessage(URL, BuildServerURL, bMsg.Author, bMsg.GetBuildRequestXML());

            Console.WriteLine("Sending build Request to Build Server after sending source Files");

            //Enqueue message in send queue for sender thread to process
            SendQ.enQ(BuildRequestMsg.GetMessage());
        }

        public void processBuildAck(ref BuildMessage bMsg)
        {
            Console.WriteLine("Build Status : " + bMsg.BuildStatus);
            //Console.WriteLine("Build Result : \n" + bMsg.BuildResult);
        }

        //
        // Test Harness Interactions
        //

        public void SendTestRequest(TestMessage tMsg)
        {
            MessageBuilder TestRequestMsg = new MessageBuilder(Message.MessageType.TestRequestMessage);
            TestRequestMsg.BuildMessage(URL, TestHarnessURL, tMsg.Author, tMsg.GetTestRequestXML());

            Console.WriteLine("Sending Test Request to Test Harness");

            //Enqueue message in send queue for sender thread to process
            SendQ.enQ(TestRequestMsg.GetMessage());
        }

        public void GetTestResult(TestMessage tMsg)
        {
            MessageBuilder TestLogMsg = new MessageBuilder(Message.MessageType.GetTestLogMsg);
            TestLogMsg.BuildMessage(URL, RepositoryURL, tMsg.Author, tMsg.QueryTestResultXML());

            Console.WriteLine("Query for Test Log for Author({0}) with Test Name({1})", tMsg.Author, tMsg.TestName);

            //Enqueue message in send queue for sender thread to process
            SendQ.enQ(TestLogMsg.GetMessage());
        }

        //
        //  Repository Interactions
        //
        public void GetAllCommitId()
        {
            MessageBuilder Msg = new MessageBuilder(Message.MessageType.RepoInfo);
            Msg.BuildMessage(URL, RepositoryURL, null, null);

            SendQ.enQ(Msg.GetMessage());
        }

        public void GetCommitInfo(ref RepoMessage rMsg)
        {
            MessageBuilder Msg = new MessageBuilder(Message.MessageType.GetCommitInfo);
            Msg.BuildMessage(URL, RepositoryURL, rMsg.Author, rMsg.GetRepoRequestXML());

            SendQ.enQ(Msg.GetMessage());
        }

        public void SendCommitRequest(ref RepoMessage rMsg)
        {
            MessageBuilder Msg = new MessageBuilder(Message.MessageType.CommitBegin);
            Msg.BuildMessage(URL, RepositoryURL, rMsg.Author, rMsg.GetRepoRequestXML());

            SendQ.enQ(Msg.GetMessage());
        }

        public void processCommitInit(ref RepoMessage rMsg)
        {
            Console.WriteLine("Commit Id : " + rMsg.CommitId);

            //Send Files to Repository
            foreach (var src in rMsg.Sources)
            {
                Console.WriteLine("Uploading file ({0}) to Repository for commit {1}", src, rMsg.CommitId);
                Streamer.UploadFile(src, rMsg.CommitId);
            }

            MessageBuilder Msg = new MessageBuilder(Message.MessageType.CommitEnd);
            Msg.BuildMessage(URL, RepositoryURL, rMsg.Author, rMsg.GetRepoRequestXML());

            SendQ.enQ(Msg.GetMessage());
        }

        public void processCommitCompleted(ref RepoMessage rMsg)
        {
            Console.WriteLine("Commit Id : " + rMsg.CommitId);
            Console.WriteLine("Commit Status : " + rMsg.CommitStatus);
            Console.WriteLine("Commit Result : \n" + rMsg.CommitResult);
        }

        public void checkout(ref RepoMessage rMsg, string location)
        {
            MessageBuilder Msg = new MessageBuilder(Message.MessageType.Checkout);
            Msg.BuildMessage(URL, RepositoryURL, rMsg.Author, rMsg.GetRepoRequestXML());

            string key = rMsg.GetHashCode().ToString();

            try {
                MessageStore.Add(key, location);    
            } catch (ArgumentException) {
                Console.WriteLine("An element with Key = {0} already exists.", key);
            }
            
            //Console.WriteLine("Saved checkout location ({0}) as key({1})", location, key);

            SendQ.enQ(Msg.GetMessage());
        }

        private void processCheckoutResp(ref RepoMessage rMsg)
        {
            string location;

            string key = rMsg.GetHashCode().ToString();
            if (MessageStore.TryGetValue(key, out location) == true)
            {
                //Console.WriteLine("Retrieved checkout Location ({0}) for key({1})", location, key);
                MessageStore.Remove(key);
            }
            else
            {
                location = checkoutDefault;
                Console.WriteLine("Failed to retrieve checkout Location for key({0}).\nDefaulting to ({1})", key, location);
            }

            if (rMsg.CommitStatus == false)
            {
                Console.WriteLine("checkout failed : " + rMsg.CommitResult);
                return;
            }

            if (!Directory.Exists(location))
            {
                Directory.CreateDirectory(location);
            }

            foreach (string file in rMsg.Sources)
	        {
                Console.WriteLine("Checkout file ({0}) from Repository to location ({1}) for commit {2}", file, location, rMsg.CommitId);
                Streamer.DownloadFile(location, file, rMsg.CommitId);
	        }

            rMsg.CommitStatus = true;
        }

        public void Quit()
        {
            MessageBuilder BuilderMsg = new MessageBuilder(Message.MessageType.Quit);
            BuilderMsg.BuildMessage(URL, BuildServerURL, null, null);
            SendQ.enQ(BuilderMsg.GetMessage());

            MessageBuilder RepoMsg = new MessageBuilder(Message.MessageType.Quit);
            RepoMsg.BuildMessage(URL, RepositoryURL, null, null);
            SendQ.enQ(RepoMsg.GetMessage());

            MessageBuilder TestMsg = new MessageBuilder(Message.MessageType.Quit);
            TestMsg.BuildMessage(URL, TestHarnessURL, null, null);
            SendQ.enQ(TestMsg.GetMessage());

            MessageBuilder ClientMsg = new MessageBuilder(Message.MessageType.Quit);
            ClientMsg.BuildMessage(URL, URL, null, null);
            SendQ.enQ(ClientMsg .GetMessage());
        }

        static void Main(string[] args)
        {
            string MyUrl = @"http://localhost:4000/ICommunication/Client";
            string TestHarnessUrl = @"http://localhost:4000/ICommunication/TestHarness";
            string RepoUrl = @"http://localhost:4000/ICommunication/Repository";
            string BuildSrvUrl = @"http://localhost:4000/ICommunication/BuildServer";
            string FileHostingUrl = @"http://localhost:4000/IStream/FileHosting";

            Client MyClient = new Client(MyUrl);
            MyClient.ConfigureURL(TestHarnessUrl, RepoUrl, BuildSrvUrl, FileHostingUrl);

            Console.Title = "Console Client";
            Thread.Sleep(1000);

            while (true)
            {
                string TestName;

                /* 1. New Test Request */
                Console.WriteLine("\nNew Test Request");
                Console.WriteLine("Enter TestName: ");
                TestName = Console.ReadLine();
                //TestName = "test1";

                TestMessage NewTestRequest = new TestMessage(TestName);

                Console.WriteLine("Enter Author: ");
                NewTestRequest.Author = Console.ReadLine();
                //NewTestRequest.Author = "Sahil Gupta";

                Console.WriteLine("Enter Driver: ");
                NewTestRequest.addDriver(Console.ReadLine());
                //NewTestRequest.addDriver(@"E:\Sahil\Syracuse\Study\CSE 681 - SMA\Project\Project 4\Repository\ClientDir\ExceptionTestDriver.dll");
                
                Console.WriteLine("Enter Code: ");
                NewTestRequest.addCode(Console.ReadLine());
                //NewTestRequest.addCode(@"E:\Sahil\Syracuse\Study\CSE 681 - SMA\Project\Project 4\Repository\ClientDir\SampleCode.dll");

                Console.WriteLine("Enter Source: ");
                NewTestRequest.addSource(Console.ReadLine());

                //MyClient.SendBuildRequest(NewTestRequest);

                MyClient.SendTestRequest(NewTestRequest);

                /* TODO : 2. Query past test */
                Console.WriteLine("\nQuery Test Results");
                Console.WriteLine("Enter TestName: ");
                TestName = Console.ReadLine();
                TestMessage NewQueryTestRequest = new TestMessage(TestName);

                Console.WriteLine("Enter Author: ");
                NewQueryTestRequest.Author = Console.ReadLine();
                MyClient.GetTestResult(NewQueryTestRequest);
            }
        }
    }
}
