﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Diagnostics;

namespace DependencyAnalyzer
{
    using System.Reflection;
    using TestInterface;

    public class DaTestDriver : MarshalByRefObject, ITest
    {
        /**********************************************************************
                                M E M B E R S
         **********************************************************************/

        DepAnal da;
        StringBuilder ResultLog;

        /**********************************************************************
                                P U B L I C   M E T H O D S
         **********************************************************************/

        public DaTestDriver()
        {
            da = new DepAnal();
            ResultLog = new StringBuilder();
        }

        /**
          * create
          * Return a new instance of TestDriver
          */
        public static ITest create()
        {
            return new DaTestDriver();
        }

        /**
         * test()
         * Implements iTest Interface function to test code
         */
        public bool test()
        {
            Console.WriteLine("Test Driver ({0}) Current AppDomain ({1})", Assembly.GetExecutingAssembly().ToString(), AppDomain.CurrentDomain.FriendlyName);

            da.BuildDepTable(@"SampleDriver\SampleDriver.cs");
            da.BuildDepTable(@"SampleCode\SampleCode.cs");
            da.BuildDepTable(@"TestInterface\ITest.cs");

            ResultLog.AppendLine("da.BuildDepTable....PASS.");

            //da.showDependencies();
            //da.showDefinitions();

            List<string> files = da.DependsOn(@"SampleDriver\SampleDriver.cs");
            ResultLog.AppendLine("da.DependsOn....PASS.");

            List<string> results = new List<string>(new string[] { "SampleCode.cs", "ITest.cs" });

            if (results.Count != files.Count)
            {
                ResultLog.AppendLine("Error: Dependency Count Mismatch");
                ResultLog.AppendLine("Expected : ");
                ResultLog.Append(results.Count);
                ResultLog.AppendLine("Found : ");
                ResultLog.Append(files.Count);
                return false;
            }

            foreach (var filename in results)
            {
                if (!files.Contains(filename))
                {
                    ResultLog.AppendLine("Error: Dependency Not Found.");
                    ResultLog.AppendLine("Expected : ");
                    ResultLog.Append(filename);
                    return false;
                }
            }
            Console.WriteLine("");
            
            ResultLog.AppendLine("DA Integrity Verified");

            return true;
        }

        /**
         * getLog
         * Implements ITest interface function to return string result for test driver
         */
        public string getLog()
        {
            return ResultLog.ToString();
        }

        static void Main(string[] args)
        {
            try
            {
                Console.WriteLine("Local test:");

                ITest test = new DaTestDriver();

                if (test.test() == true)
                {
                    Console.WriteLine("\n>>>Test PASS<<<");
                }
                else
                {
                    Console.WriteLine("\n>>>Test FAIL<<<");
                }

                Console.WriteLine("\nTest Logs:");
                Console.WriteLine("{0}", test.getLog());
            }
            catch (Exception Ex)
            {
                Console.WriteLine("Exception : {0}", Ex.Message);
            }
        }
    }
}
